package com.asokorea.essm.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DataSourceConfig {
	
	@Primary
	@Bean(name = "metaDs", destroyMethod = "close")
	@ConfigurationProperties(prefix = "meta.datasource.hikari")
	public DataSource metaDs() {
		return DataSourceBuilder.create()
				.type(HikariDataSource.class)
				.build();
	}

	@Bean(name = "essmDs", destroyMethod = "close")
	@ConfigurationProperties(prefix = "essm.datasource.hikari")
	public DataSource essmDs() {
		return DataSourceBuilder.create()
				.type(HikariDataSource.class)
				.build();
	}

}
