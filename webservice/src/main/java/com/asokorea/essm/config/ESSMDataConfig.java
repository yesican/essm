package com.asokorea.essm.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@MapperScan(basePackages = "com.asokorea.essm.common.biz.dao.essm", sqlSessionFactoryRef = "essmSqlSessionFactory")
public class ESSMDataConfig {

	@Primary
	@Bean(name = "essmSqlSessionFactory")
	public SqlSessionFactory metaSqlSessionFactory(@Qualifier("essmDs") DataSource ds,
			ApplicationContext applicationContext) throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(ds);
		return sqlSessionFactoryBean.getObject();
	}

	@Primary
	@Bean
	public SqlSessionTemplate metaSqlSessionTemplate(
			@Qualifier("metaSqlSessionFactory") SqlSessionFactory sqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(sqlSessionFactory);
	}

}
