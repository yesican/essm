package com.asokorea.essm.webservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asokorea.essm.common.biz.dto.UserDTO;
import com.asokorea.essm.common.biz.service.AdminService;
import com.asokorea.essm.common.core.SharedClass;

@RestController
public class HelloController {

	@Autowired
	private AdminService adminService;

	@RequestMapping("/hello")
	public String Hello() throws Exception {
		return "Hello World! " + SharedClass.getMessage();
	}

	@RequestMapping("/hi")
	public List<UserDTO> hi() throws Exception {
		return adminService.getUserList(1, "Y", null);
	}

}
