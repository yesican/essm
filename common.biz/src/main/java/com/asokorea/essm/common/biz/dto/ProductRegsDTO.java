package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ProductRegsDTO")
public class ProductRegsDTO {
	
	private int prdtMstrSeqc;
	private int srvcSeqc;
	private int mainAcntSeqc;
	private String cltsKind;
	private String prdtNm;
	private String frstBuyDay;
	private int frstBuyPrce;
	private int sellPrce;
	private int tagPrtgPrce;
	private String year;
	private String sesn;
	private String pintAccmYn;
	private String sellStts;
	private String imgFileNm;
	private String refcCntt;
	private String acntNm;
	private String cltsKindNm;
	private String sellSttsNm;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getFrstBuyDay() {
		return frstBuyDay;
	}
	public void setFrstBuyDay(String frstBuyDay) {
		this.frstBuyDay = frstBuyDay;
	}
	public int getFrstBuyPrce() {
		return frstBuyPrce;
	}
	public void setFrstBuyPrce(int frstBuyPrce) {
		this.frstBuyPrce = frstBuyPrce;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public int getTagPrtgPrce() {
		return tagPrtgPrce;
	}
	public void setTagPrtgPrce(int tagPrtgPrce) {
		this.tagPrtgPrce = tagPrtgPrce;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getSesn() {
		return sesn;
	}
	public void setSesn(String sesn) {
		this.sesn = sesn;
	}
	public String getPintAccmYn() {
		return pintAccmYn;
	}
	public void setPintAccmYn(String pintAccmYn) {
		this.pintAccmYn = pintAccmYn;
	}
	public String getSellStts() {
		return sellStts;
	}
	public void setSellStts(String sellStts) {
		this.sellStts = sellStts;
	}
	public String getImgFileNm() {
		return imgFileNm;
	}
	public void setImgFileNm(String imgFileNm) {
		this.imgFileNm = imgFileNm;
	}
	public String getRefcCntt() {
		return refcCntt;
	}
	public void setRefcCntt(String refcCntt) {
		this.refcCntt = refcCntt;
	}
	public String getAcntNm() {
		return acntNm;
	}
	public void setAcntNm(String acntNm) {
		this.acntNm = acntNm;
	}
	public String getCltsKindNm() {
		return cltsKindNm;
	}
	public void setCltsKindNm(String cltsKindNm) {
		this.cltsKindNm = cltsKindNm;
	}
	public String getSellSttsNm() {
		return sellSttsNm;
	}
	public void setSellSttsNm(String sellSttsNm) {
		this.sellSttsNm = sellSttsNm;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	
	
}
