package com.asokorea.essm.common.biz.dao.meta;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.asokorea.essm.common.biz.dto.ServiceDTO;
import com.asokorea.essm.common.biz.dto.UserDTO;

@Mapper
public interface MetaMapper {

	@Select("select NOW()")
	public Date getMetaDBDate() throws Exception;

	public List<UserDTO> getUserList(@Param("srvcSeqc") int srvcSeqc
			, @Param("includeUnusedYN") String includeUnusedYN
			, @Param("userId") String userId) throws Exception;

	public int addUser(@Param("userDTO") UserDTO userDTO) throws Exception;

	public int setUser(@Param("userDTO") UserDTO userDTO) throws Exception;

	public ServiceDTO getService(@Param("srvcSeqc") int srvcSeqc, @Param("userId") String userId) throws Exception;

	public int setService(@Param("serviceDTO") ServiceDTO serviceDTO) throws Exception;

	public ServiceDTO getServiceByUserId(@Param("userId") String userId) throws Exception;

	public UserDTO getUser(@Param("userSeqc") int userSeqc, @Param("userId") String userId) throws Exception;

	public int changePassword(@Param("userId") String userId
			, @Param("oldPassword") String oldPassword
			, @Param("newPassword") String newPassword) throws Exception;

	public void setCounter(Map<String, Object> map) throws Exception;

}