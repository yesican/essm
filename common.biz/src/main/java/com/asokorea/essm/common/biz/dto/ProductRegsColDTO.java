package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ProductRegsColDTO")
public class ProductRegsColDTO {

	private int prdtMstrSeqc;
	private String clor;
	private String oldColor;
	
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getOldColor() {
		return oldColor;
	}
	public void setOldColor(String oldColor) {
		this.oldColor = oldColor;
	}
}
