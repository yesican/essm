package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="EventProductDTO")
public class EventProductDTO {
	
	private int evntSeqc;
	private int prdtMstrSeqc;
	private int ttalBuyCnt;
	private int ttalSellCnt;
	private int ttalStock;
	private int evntPrce;
	private int sellPrce;
	private String prdtNm;
	private String acntSeqc;
	private String acntNm;
	
	public int getEvntSeqc() {
		return evntSeqc;
	}
	public void setEvntSeqc(int evntSeqc) {
		this.evntSeqc = evntSeqc;
	}
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public int getTtalBuyCnt() {
		return ttalBuyCnt;
	}
	public void setTtalBuyCnt(int ttalBuyCnt) {
		this.ttalBuyCnt = ttalBuyCnt;
	}
	public int getTtalSellCnt() {
		return ttalSellCnt;
	}
	public void setTtalSellCnt(int ttalSellCnt) {
		this.ttalSellCnt = ttalSellCnt;
	}
	public int getTtalStock() {
		return ttalStock;
	}
	public void setTtalStock(int ttalStock) {
		this.ttalStock = ttalStock;
	}
	public int getEvntPrce() {
		return evntPrce;
	}
	public void setEvntPrce(int evntPrce) {
		this.evntPrce = evntPrce;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int salePrce) {
		this.sellPrce = salePrce;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getAcntSeqc() {
		return acntSeqc;
	}
	public void setAcntSeqc(String acntSeqc) {
		this.acntSeqc = acntSeqc;
	}
	public String getAcntNm() {
		return acntNm;
	}
	public void setAcntNm(String acntNm) {
		this.acntNm = acntNm;
	}

}
