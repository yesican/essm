package com.asokorea.essm.common.biz.service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asokorea.essm.common.biz.dao.essm.ProductMapper;
import com.asokorea.essm.common.biz.dto.EventDTO;
import com.asokorea.essm.common.biz.dto.EventProductDTO;
import com.asokorea.essm.common.biz.dto.EventProductSearchDTO;
import com.asokorea.essm.common.biz.dto.MyOptionClorDTO;
import com.asokorea.essm.common.biz.dto.MyOptionDTO;
import com.asokorea.essm.common.biz.dto.MyOptionDetailDTO;
import com.asokorea.essm.common.biz.dto.MyOptionSizeDTO;
import com.asokorea.essm.common.biz.dto.ProductDetailsRegsDTO;
import com.asokorea.essm.common.biz.dto.ProductRegsColDTO;
import com.asokorea.essm.common.biz.dto.ProductRegsDTO;
import com.asokorea.essm.common.biz.dto.ProductRegsSizeDTO;
import com.asokorea.essm.common.biz.dto.ProductSearchListDTO;
import com.asokorea.essm.common.biz.support.PropertyHelper;
import com.asokorea.essm.common.biz.support.SessionHelper;
import com.asokorea.essm.common.biz.support.Util;

@Service
public class ProductService {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private ProductMapper productMapper;

	public int addEvent(@WebParam(name = "eventDTO") EventDTO eventDTO,
			@WebParam(name = "eventProductList") EventProductDTO[] eventProductList) throws Exception {

		int result = 0;
		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		eventDTO.setSrvcSeqc(srvcSeqc);
		productMapper.addEvent(eventDTO);

		int evntSeqc = eventDTO.getEvntSeqc();

		if (evntSeqc > 0) {
			if (eventProductList != null && eventProductList.length > 0) {
				for (EventProductDTO eventProductDTO : eventProductList) {
					eventProductDTO.setEvntSeqc(evntSeqc);
					productMapper.addEventProduct(eventProductDTO);
				}
			}
			result = evntSeqc;
		} else {
			result = 0;
			throw new Exception("신규 이벤트 생성 오류");
		}

		return result;
	}

	public List<EventDTO> getEvent(@WebParam(name = "eventName") String eventName,
			@WebParam(name = "includeEndEventYN") String includeEndEventYN) throws Exception {
		List<EventDTO> list = null;
		list = productMapper.getEvent(eventName, includeEndEventYN);
		return list;
	}

	public List<EventProductDTO> getEventProduct(@WebParam(name = "evntSeqc") int evntSeqc) throws Exception {
		List<EventProductDTO> list = null;
		list = productMapper.getEventProduct(evntSeqc);
		return list;
	}

	public List<EventProductDTO> getUnselectEventProduct(
			@WebParam(name = "eventProductSearchDTO") EventProductSearchDTO eventProductSearchDTO) throws Exception {
		List<EventProductDTO> list = null;
		list = productMapper.getUnselectEventProduct(eventProductSearchDTO);
		return list;
	}

	public int delEventProduct(@WebParam(name = "eventProductDTOList") EventProductDTO[] eventProductDTOList)
			throws Exception {

		int result = 0;

		int evntSeqc = 0;
		int prdtMstrSeqc = 0;

		for (EventProductDTO eventProductDTO : eventProductDTOList) {
			evntSeqc = eventProductDTO.getEvntSeqc();
			prdtMstrSeqc = eventProductDTO.getPrdtMstrSeqc();
			result += productMapper.delEventProduct(evntSeqc, prdtMstrSeqc);
		}

		return result;
	}

	public int setEvent(@WebParam(name = "eventDTO") EventDTO eventDTO,
			@WebParam(name = "productList") EventProductDTO[] productList) throws Exception {

		int result = 0;
		result += productMapper.setEvent(eventDTO);

		for (EventProductDTO eventProductDTO : productList) {
			result += productMapper.setEventProduct(eventDTO, eventProductDTO);
		}

		return result;
	}

	public List<ProductRegsDTO> getProductRegList(@WebParam(name = "startDate") String startDate,
			@WebParam(name = "endDate") String endDate, @WebParam(name = "mainAcntSeqc") int mainAcntSeqc,
			@WebParam(name = "srvcSeqc") int srvcSeqc, @WebParam(name = "prdtNm") String prdtNm,
			@WebParam(name = "cltsKind") String cltsKind, @WebParam(name = "sellSttsYN") String sellSttsYN)
			throws Exception {
		List<ProductRegsDTO> list = null;
		list = productMapper.getProductRegList(startDate, endDate, mainAcntSeqc, srvcSeqc, prdtNm, cltsKind,
				sellSttsYN);
		return list;
	}

	public List<ProductRegsColDTO> productRegsGetColList(@WebParam(name = "prdtMstrSeqc") int prdtMstrSeqc)
			throws Exception {
		List<ProductRegsColDTO> list = null;
		list = productMapper.productRegsGetColList(prdtMstrSeqc);
		return list;
	}

	public List<ProductRegsSizeDTO> productRegsGetSizeList(@WebParam(name = "prdtMstrSeqc") int prdtMstrSeqc)
			throws Exception {
		List<ProductRegsSizeDTO> list = null;
		list = productMapper.productRegsGetSizeList(prdtMstrSeqc);
		return list;
	}

	public int addProduct(@WebParam(name = "productRegsDTO") ProductRegsDTO productRegsDTO, byte[] imageData)
			throws Exception {

		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		String userId = SessionHelper.getLoginDTO(request).getUserId();
		productRegsDTO.setSrvcSeqc(srvcSeqc);
		productRegsDTO.setRegsUserId(userId);
		productRegsDTO.setUpdtUserId(userId);
		productMapper.addProduct(productRegsDTO);
		int prdtMstrSeqc = productRegsDTO.getPrdtMstrSeqc();

		if (prdtMstrSeqc > 0 && imageData != null && imageData.length > 0) {
			Path imageSavePath = Paths.get(PropertyHelper.imageRootPath, String.valueOf(srvcSeqc));
			String imageSaveDir = imageSavePath.toString();
			Util.saveProductImage(prdtMstrSeqc, imageSaveDir, imageData);
		}

		return prdtMstrSeqc;
	}

	public int setProduct(@WebParam(name = "productRegsDTO") ProductRegsDTO productRegsDTO, byte[] imageData)
			throws Exception {

		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		String userId = SessionHelper.getLoginDTO(request).getUserId();
		int prdtMstrSeqc = productRegsDTO.getPrdtMstrSeqc();
		String fileName = productRegsDTO.getImgFileNm();
		productRegsDTO.setSrvcSeqc(srvcSeqc);
		productRegsDTO.setUpdtUserId(userId);

		int result = productMapper.setProduct(productRegsDTO);

		if (result > 0) {

			Path imageSavePath = Paths.get(PropertyHelper.imageRootPath, String.valueOf(srvcSeqc));
			String imageSaveDir = imageSavePath.toString();

			if (fileName != null && imageData != null && imageData.length > 0) {
				// 파일명과 데이타가 모두 있을때 이미지 교체
				Util.saveProductImage(prdtMstrSeqc, imageSaveDir, imageData);
			} else if (fileName == null && (imageData == null || imageData.length == 0)) {
				// 파일명과 데이타 모두 없을때 이미지 삭제
				Util.deleteProductImage(prdtMstrSeqc, imageSaveDir);
			}
		}

		return result;
	}

	public int setProductRegsDetail(ProductDetailsRegsDTO[] setProductDetailList) throws Exception {

		int result = 0;

		for (ProductDetailsRegsDTO productDetailsRegsDTO : setProductDetailList) {
			result += productMapper.setProductRegsDetail(productDetailsRegsDTO);
		}

		return result;
	}

	public int addProductRegsDetail(ProductDetailsRegsDTO[] addProductDetailList) throws Exception {

		int result = 0;

		for (ProductDetailsRegsDTO productDetailsRegsDTO : addProductDetailList) {
			result += productMapper.addProductRegsDetail(productDetailsRegsDTO);
		}

		return result;
	}

	public List<ProductRegsColDTO> productRegsGetNewColList() throws Exception {
		List<ProductRegsColDTO> list = null;
		list = productMapper.productRegsGetNewColList();
		return list;
	}

	public List<ProductRegsSizeDTO> productRegsGetNewSizeList() throws Exception {
		List<ProductRegsSizeDTO> list = null;
		list = productMapper.productRegsGetNewSizeList();
		return list;
	}

	public List<MyOptionDTO> getMyOtion(@WebParam(name = "srvcSeqc") int srvcSeqc) throws Exception {
		List<MyOptionDTO> list = null;
		list = productMapper.getMyOtion(srvcSeqc);
		return list;
	}

	public List<MyOptionClorDTO> getMyOtionCol(@WebParam(name = "srvcSeqc") int srvcSeqc) throws Exception {
		List<MyOptionClorDTO> list = null;
		list = productMapper.getMyOtionCol(srvcSeqc);
		return list;
	}

	public List<MyOptionSizeDTO> getMyOtionSize(@WebParam(name = "srvcSeqc") int srvcSeqc) throws Exception {
		List<MyOptionSizeDTO> list = null;
		list = productMapper.getMyOtionSize(srvcSeqc);
		return list;
	}

	public int addMyOtion(@WebParam(name = "myOptionDTO") MyOptionDTO myOptionDTO) throws Exception {

		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		String userId = SessionHelper.getLoginDTO(request).getUserId();
		myOptionDTO.setSrvcSeqc(srvcSeqc);
		myOptionDTO.setUseYn("Y");
		myOptionDTO.setRegsUserId(userId);
		myOptionDTO.setUpdtUserId(userId);
		productMapper.addMyOtion(myOptionDTO);
		int result = myOptionDTO.getMyOptnSeqc();
		return result;
	}

	public int setMyOtion(@WebParam(name = "myOptionDTO") MyOptionDTO myOptionDTO) throws Exception {

		int result = productMapper.setMyOtion(myOptionDTO);
		return result;
	}

	public int delMyOtion(@WebParam(name = "myOptnSeqc") int myOptnSeqc) throws Exception {
		int result = 0;
		result += productMapper.delMyOtionDetail(myOptnSeqc);
		result += productMapper.delMyOtion(myOptnSeqc);

		return result;
	}

	public int addMyOtionDetail(@WebParam(name = "myOptionDetailList") MyOptionDetailDTO[] myOptionDetailList,
			@WebParam(name = "myOptnSeqc") int myOptnSeqc) throws Exception {

		int result = 0;
		result += productMapper.delMyOtionDetail(myOptnSeqc);

		for (MyOptionDetailDTO myOptionDetailDTO : myOptionDetailList) {
			result += productMapper.addMyOtionDetail(myOptionDetailDTO, myOptnSeqc);
		}

		return result;
	}

	public List<ProductSearchListDTO> getProductSearchList(@WebParam(name = "srvcSeqc") int srvcSeqc,
			@WebParam(name = "barcode") String barcode, @WebParam(name = "prdtNm") String prdtNm) throws Exception {
		List<ProductSearchListDTO> result = null;
		result = productMapper.getProductSearchList(srvcSeqc, barcode, prdtNm);
		return result;
	}

	public byte[] getProductImage(@WebParam(name = "prdtMstrSeqc") int prdtMstrSeqc) throws Exception {
		byte[] result = null;
		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		Path imageSavePath = Paths.get(PropertyHelper.imageRootPath, String.valueOf(srvcSeqc));
		String imageSaveDir = imageSavePath.toString();
		result = Util.getProductImage(prdtMstrSeqc, imageSaveDir);
		return result;
	}
}
