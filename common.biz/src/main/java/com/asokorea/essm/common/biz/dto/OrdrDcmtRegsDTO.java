package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="OrdrDcmtRegsDTO")
public class OrdrDcmtRegsDTO {
	
	private int ordrDcmt;		//주문서순번
	private int srvcSeqc;		//서비스순번
	private String ordrDay;		//주문일자
	//private String ordrCmptYn;	//주문완료여부
	private String useYn;		//사용여부
	private String regsUserId;	//최초 등록자 아이디
	private String updtUserId;	//최종 수정자 아이디
	
	public int getOrdrDcmt() {
		return ordrDcmt;
	}
	public void setOrdrDcmt(int ordrDcmt) {
		this.ordrDcmt = ordrDcmt;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getOrdrDay() {
		return ordrDay;
	}
	public void setOrdrDay(String ordrDay) {
		this.ordrDay = ordrDay;
	}
//	public String getOrdrCmptYn() {
//		return ordrCmptYn;
//	}
//	public void setOrdrCmptYn(String ordrCmptYn) {
//		this.ordrCmptYn = ordrCmptYn;
//	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	
	
}
