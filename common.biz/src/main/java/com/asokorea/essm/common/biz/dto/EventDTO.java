package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="EventDTO")
public class EventDTO {
	
	private int srvcSeqc;
	private int evntSeqc;
	private String evntNm;
	private String strtDay;
	private String endDay;
	private String prgsStts;
	private String prgsSttsLbel;
	
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public int getEvntSeqc() {
		return evntSeqc;
	}
	public void setEvntSeqc(int evntSeqc) {
		this.evntSeqc = evntSeqc;
	}
	public String getEvntNm() {
		return evntNm;
	}
	public void setEvntNm(String evntNm) {
		this.evntNm = evntNm;
	}
	public String getStrtDay() {
		return strtDay;
	}
	public void setStrtDay(String strtDay) {
		this.strtDay = strtDay;
	}
	public String getEndDay() {
		return endDay;
	}
	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}
	public String getPrgsStts() {
		return prgsStts;
	}
	public void setPrgsStts(String prgsStts) {
		this.prgsStts = prgsStts;
	}
	public String getPrgsSttsLbel() {
		return prgsSttsLbel;
	}
	public void setPrgsSttsLbel(String prgsSttsLbel) {
		this.prgsSttsLbel = prgsSttsLbel;
	}
}
