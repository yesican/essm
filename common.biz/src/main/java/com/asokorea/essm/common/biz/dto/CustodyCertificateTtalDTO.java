package com.asokorea.essm.common.biz.dto;


import javax.xml.bind.annotation.XmlType;

@XmlType(name = "CustodyCertificateTtalDTO")
public class CustodyCertificateTtalDTO {

	private String yyyymm;
	private int markMonyTtal;			//보관증발행총액
	private int cstdCtfcUseMonyTtal;	//보관증사용총액
	private int cltnMonyTtal;			//보관증회수총액
	private int useExpnMonyTtal;		//보관증미사용만료액
	
	public String getYyyymm() {
		return yyyymm;
	}
	public void setYyyymm(String yyyymm) {
		this.yyyymm = yyyymm;
	}
	public int getMarkMonyTtal() {
		return markMonyTtal;
	}
	public void setMarkMonyTtal(int markMonyTtal) {
		this.markMonyTtal = markMonyTtal;
	}
	public int getCstdCtfcUseMonyTtal() {
		return cstdCtfcUseMonyTtal;
	}
	public void setCstdCtfcUseMonyTtal(int cstdCtfcUseMonyTtal) {
		this.cstdCtfcUseMonyTtal = cstdCtfcUseMonyTtal;
	}
	public int getCltnMonyTtal() {
		return cltnMonyTtal;
	}
	public void setCltnMonyTtal(int cltnMonyTtal) {
		this.cltnMonyTtal = cltnMonyTtal;
	}
	public int getUseExpnMonyTtal() {
		return useExpnMonyTtal;
	}
	public void setUseExpnMonyTtal(int useExpnMonyTtal) {
		this.useExpnMonyTtal = useExpnMonyTtal;
	}
}
