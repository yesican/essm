package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="CreditDTO")
public class CreditDTO {

	private int ctmrCrdtSeqc;
	private int ctmrSeqc;
	private int sellSeqc;
	private String crdtRpayDvsn;
	private int mony;
	private String crdtRpayNote;
	private String ocrcDay;
	
	public int getCtmrCrdtSeqc() {
		return ctmrCrdtSeqc;
	}
	public void setCtmrCrdtSeqc(int ctmrCrdtSeqc) {
		this.ctmrCrdtSeqc = ctmrCrdtSeqc;
	}
	public int getCtmrSeqc() {
		return ctmrSeqc;
	}
	public void setCtmrSeqc(int ctmrSeqc) {
		this.ctmrSeqc = ctmrSeqc;
	}
	public int getSellSeqc() {
		return sellSeqc;
	}
	public void setSellSeqc(int sellSeqc) {
		this.sellSeqc = sellSeqc;
	}
	public String getCrdtRpayDvsn() {
		return crdtRpayDvsn;
	}
	public void setCrdtRpayDvsn(String crdtRpayDvsn) {
		this.crdtRpayDvsn = crdtRpayDvsn;
	}
	public int getMony() {
		return mony;
	}
	public void setMony(int mony) {
		this.mony = mony;
	}
	public String getCrdtRpayNote() {
		return crdtRpayNote;
	}
	public void setCrdtRpayNote(String crdtRpayNote) {
		this.crdtRpayNote = crdtRpayNote;
	}
	public String getOcrcDay() {
		return ocrcDay;
	}
	public void setOcrcDay(String ocrcDay) {
		this.ocrcDay = ocrcDay;
	}
}
