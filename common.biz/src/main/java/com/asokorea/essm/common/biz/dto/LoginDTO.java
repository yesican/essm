
package com.asokorea.essm.common.biz.dto;

import lombok.Data;

@Data
public class LoginDTO {

	private String loginMode;
	private String userId;
	private int srvcSeqc;
	private ServiceDTO serviceDTO;
	private UserDTO userDTO;
	private int loginResult;	// 0:정상, 1:아이디 없음, 2:패스워드 틀림, 3:사용불가(미결제, 휴면, 불량)
	private int counterNo;

}
