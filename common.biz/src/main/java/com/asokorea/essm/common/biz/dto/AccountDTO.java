package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "AccountDTO")
public class AccountDTO {

	private int acntSeqc;
	private int srvcSeqc;
	private String acntNm;
	private String acntDvsn;
	private String shopNm;
	private String rptvPsonNm;
	private String licsNum;
	private String bnssShpe;
	private String bnssKind;
	private String rptvPhne;
	private String rptvMail;
	private String chrgPsonNm;
	private String chrgPsonPhne;
	private String cmpnAddr;
	private String shopLocn;
	private String dealStts;
	private String refcCntt;
	private String regsDay;
	private String bankNm;
	private String acntOwnr;
	private String acntNum;
	private int ttalBuyMony;
	private int ttalPaymMony;
	private int ttalUnpaymMony;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getAcntSeqc() {
		return acntSeqc;
	}
	public void setAcntSeqc(int acntSeqc) {
		this.acntSeqc = acntSeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getAcntNm() {
		return acntNm;
	}
	public void setAcntNm(String acntNm) {
		this.acntNm = acntNm;
	}
	public String getAcntDvsn() {
		return acntDvsn;
	}
	public void setAcntDvsn(String acntDvsn) {
		this.acntDvsn = acntDvsn;
	}
	public String getShopNm() {
		return shopNm;
	}
	public void setShopNm(String shopNm) {
		this.shopNm = shopNm;
	}
	public String getRptvPsonNm() {
		return rptvPsonNm;
	}
	public void setRptvPsonNm(String rptvPsonNm) {
		this.rptvPsonNm = rptvPsonNm;
	}
	public String getLicsNum() {
		return licsNum;
	}
	public void setLicsNum(String licsNum) {
		this.licsNum = licsNum;
	}
	public String getBnssShpe() {
		return bnssShpe;
	}
	public void setBnssShpe(String bnssShpe) {
		this.bnssShpe = bnssShpe;
	}
	public String getBnssKind() {
		return bnssKind;
	}
	public void setBnssKind(String bnssKind) {
		this.bnssKind = bnssKind;
	}
	public String getRptvPhne() {
		return rptvPhne;
	}
	public void setRptvPhne(String rptvPhne) {
		this.rptvPhne = rptvPhne;
	}
	public String getRptvMail() {
		return rptvMail;
	}
	public void setRptvMail(String rptvMail) {
		this.rptvMail = rptvMail;
	}
	public String getChrgPsonNm() {
		return chrgPsonNm;
	}
	public void setChrgPsonNm(String chrgPsonNm) {
		this.chrgPsonNm = chrgPsonNm;
	}
	public String getChrgPsonPhne() {
		return chrgPsonPhne;
	}
	public void setChrgPsonPhne(String chrgPsonPhne) {
		this.chrgPsonPhne = chrgPsonPhne;
	}
	public String getCmpnAddr() {
		return cmpnAddr;
	}
	public void setCmpnAddr(String cmpnAddr) {
		this.cmpnAddr = cmpnAddr;
	}
	public String getShopLocn() {
		return shopLocn;
	}
	public void setShopLocn(String shopLocn) {
		this.shopLocn = shopLocn;
	}
	public String getDealStts() {
		return dealStts;
	}
	public void setDealStts(String dealStts) {
		this.dealStts = dealStts;
	}
	public String getRefcCntt() {
		return refcCntt;
	}
	public void setRefcCntt(String refcCntt) {
		this.refcCntt = refcCntt;
	}
	public String getRegsDay() {
		return regsDay;
	}
	public void setRegsDay(String regsDay) {
		this.regsDay = regsDay;
	}
	public String getBankNm() {
		return bankNm;
	}
	public void setBankNm(String bankNm) {
		this.bankNm = bankNm;
	}
	public String getAcntOwnr() {
		return acntOwnr;
	}
	public void setAcntOwnr(String acntOwnr) {
		this.acntOwnr = acntOwnr;
	}
	public String getAcntNum() {
		return acntNum;
	}
	public void setAcntNum(String acntNum) {
		this.acntNum = acntNum;
	}
	public int getTtalBuyMony() {
		return ttalBuyMony;
	}
	public void setTtalBuyMony(int ttalBuyMony) {
		this.ttalBuyMony = ttalBuyMony;
	}
	public int getTtalPaymMony() {
		return ttalPaymMony;
	}
	public void setTtalPaymMony(int ttalPaymMony) {
		this.ttalPaymMony = ttalPaymMony;
	}
	public int getTtalUnpaymMony() {
		return ttalUnpaymMony;
	}
	public void setTtalUnpaymMony(int ttalUnpaymMony) {
		this.ttalUnpaymMony = ttalUnpaymMony;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
}
