package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "StckAjmtDetailRegsDTO")
public class StckAjmtDetailRegsDTO {

	private int stckAjmtSeqc;		//재고조정순번
	private int stckAjmtDtlSeqc;	//재고조정상세순번
	private int seqc;				//순번
	private int prdtSeqc;			//상품순번
	private int stckAjmtQntt;		//재고조사 수량
	private String prdtNm;			//상품명
	private String clor;			//칼라
	private String size;			//사이즈
	private int mainAcntSeqc;		//주거래처
	private String cltsKind;			//품종
	private String frstBuyDay;		//첫사입일
	private int sellPrce;			//판매가
	private int stckQntt;			//전산재고
	private int gapQntt;			//차이수량
	private int sellQkntt;			//판매수량
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getStckAjmtSeqc() {
		return stckAjmtSeqc;
	}
	public void setStckAjmtSeqc(int stckAjmtSeqc) {
		this.stckAjmtSeqc = stckAjmtSeqc;
	}
	public int getStckAjmtDtlSeqc() {
		return stckAjmtDtlSeqc;
	}
	public void setStckAjmtDtlSeqc(int stckAjmtDtlSeqc) {
		this.stckAjmtDtlSeqc = stckAjmtDtlSeqc;
	}
	public int getSeqc() {
		return seqc;
	}
	public void setSeqc(int seqc) {
		this.seqc = seqc;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public int getStckAjmtQntt() {
		return stckAjmtQntt;
	}
	public void setStckAjmtQntt(int stckAjmtQntt) {
		this.stckAjmtQntt = stckAjmtQntt;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getFrstBuyDay() {
		return frstBuyDay;
	}
	public void setFrstBuyDay(String frstBuyDay) {
		this.frstBuyDay = frstBuyDay;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public int getStckQkntt() {
		return stckQntt;
	}
	public void setStckQkntt(int stckQntt) {
		this.stckQntt = stckQntt;
	}
	public int getGapQntt() {
		return gapQntt;
	}
	public void setGapQntt(int gapQntt) {
		this.gapQntt = gapQntt;
	}
	public int getSellQkntt() {
		return sellQkntt;
	}
	public void setSellQkntt(int sellQkntt) {
		this.sellQkntt = sellQkntt;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
}
