package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="YearRangeDTO")
public class YearRangeDTO {

	private int minYear;
	private int maxYear;

	public int getMinYear() {
		return minYear;
	}
	public void setMinYear(int minYear) {
		this.minYear = minYear;
	}
	public int getMaxYear() {
		return maxYear;
	}
	public void setMaxYear(int maxYear) {
		this.maxYear = maxYear;
	}

}
