package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="OrdrRegsDTO")
public class OrdrRegsDTO {
	
	private String selected; //선택여부
	private int ordrDcmt;	//주문서순번
	private int prdtSeqc;	//상품순번
	private String prdtNm;	//상품명
	private String clor;	//칼라
	private String size;	//사이즈
	private int mainAcntSeqc;	//주거래처
	private String cltsKind;	//품종
	private int ttalSellQntt;	//판매수량
	private int ttalStckQntt;	//재고수량
	private int ordrQntt;	//주문수량
	private String ordrCmptYn;	//주문완료여부
	private String useYn;	//사용여부
	private String regsUserId;	//최초 등록자 아이디
	private String updtUserId;	//최종 수정자 아이디
	
	public String getSelected() {
		return selected;
	}
	public void setSelected(String selected) {
		this.selected = selected;
	}
	public int getOrdrDcmt() {
		return ordrDcmt;
	}
	public void setOrdrDcmt(int ordrDcmt) {
		this.ordrDcmt = ordrDcmt;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public int getTtalSellQntt() {
		return ttalSellQntt;
	}
	public void setTtalSellQntt(int ttalSellQntt) {
		this.ttalSellQntt = ttalSellQntt;
	}
	public int getTtalStckQntt() {
		return ttalStckQntt;
	}
	public void setTtalStckQntt(int ttalStckQntt) {
		this.ttalStckQntt = ttalStckQntt;
	}
	public int getOrdrQntt() {
		return ordrQntt;
	}
	public void setOrdrQntt(int ordrQntt) {
		this.ordrQntt = ordrQntt;
	}
	public String getOrdrCmptYn() {
	return ordrCmptYn;
	}
	public void setOrdrCmptYn(String ordrCmptYn) {
		this.ordrCmptYn = ordrCmptYn;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
}
