package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ProductOrderDTO")
public class ProductOrderDTO {
	
	private int sellSeqc;
	private int prdtMstrSeqc;
	private int prdtSeqc;
	private int mainAcntSeqc;
	private String cltsKind;
	private String prdtNm;
	private String frstBuyDay;
	private String clor;
	private String size;
	private int unitPrce;
	private int ttalStckQntt;
	private int evntSeqc;
	private int sellPrce;
	private int sellQntt;
	private int dcntRate;
	private String barcode;
	
	public int getSellSeqc() {
		return sellSeqc;
	}
	public void setSellSeqc(int sellSeqc) {
		this.sellSeqc = sellSeqc;
	}
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getFrstBuyDay() {
		return frstBuyDay;
	}
	public void setFrstBuyDay(String frstBuyDay) {
		this.frstBuyDay = frstBuyDay;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getUnitPrce() {
		return unitPrce;
	}
	public void setUnitPrce(int unitPrce) {
		this.unitPrce = unitPrce;
	}
	public int getTtalStckQntt() {
		return ttalStckQntt;
	}
	public void setTtalStckQntt(int ttalStckQntt) {
		this.ttalStckQntt = ttalStckQntt;
	}
	public int getEvntSeqc() {
		return evntSeqc;
	}
	public void setEvntSeqc(int evntSeqc) {
		this.evntSeqc = evntSeqc;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public int getSellQntt() {
		return sellQntt;
	}
	public void setSellQntt(int sellQntt) {
		this.sellQntt = sellQntt;
	}
	public int getDcntRate() {
		return dcntRate;
	}
	public void setDcntRate(int dcntRate) {
		this.dcntRate = dcntRate;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

}
