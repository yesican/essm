package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import lombok.Data;

@Data
public class PaymentDTO {

	private int sellSeqc;
	private String paymDvsn;
	private int paymMony;
	private String aprvNum;
	private String crdtCardNum;
	private String halbu;
	private String isCorporateYn;
	private String icCardYn;
	private String aprvDate;
	private String cardIssueComp;
	private String halbuName;
	private String posMember;
	private String purchaseComp;
	private String rawData;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;

}
