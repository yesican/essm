package com.asokorea.essm.common.biz.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asokorea.essm.common.biz.dao.essm.AdminMapper;
import com.asokorea.essm.common.biz.dao.meta.MetaMapper;
import com.asokorea.essm.common.biz.dto.AccountDTO;
import com.asokorea.essm.common.biz.dto.SellMntcCostDayTtalDTO;
import com.asokorea.essm.common.biz.dto.SellMntcCostDetailDTO;
import com.asokorea.essm.common.biz.dto.SellMntcCostTtalDTO;
import com.asokorea.essm.common.biz.dto.ServiceDTO;
import com.asokorea.essm.common.biz.dto.UserDTO;
import com.asokorea.essm.common.biz.support.SessionHelper;

@Service
public class AdminService {

	@Autowired
	private HttpServletRequest request;	
	
	@Autowired
	private AdminMapper adminMapper;
	
	@Autowired
	private MetaMapper metaMapper;

	public Date getMetaDBDate() throws Exception {
		Date date = metaMapper.getMetaDBDate();
		return date;
	}
	
	public List<AccountDTO> getAccountList(@WebParam(name = "acntNm") String acntNm,
			@WebParam(name = "acntDvsn") String acntDvsn,
			@WebParam(name = "includeEndYN") String includeEndYN) throws Exception {
		List<AccountDTO> result = null;
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		result = adminMapper.getAccountList(srvcSeqc, acntNm, acntDvsn, includeEndYN);
		return result;
	}
	
	public int addAccount(@WebParam(name = "accountDTO") AccountDTO accountDTO) throws Exception {
		
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("accountDTO", accountDTO);
		adminMapper.addAccount(map);		
		int result = accountDTO.getAcntSeqc();
		return result;
		
	}
	
	public int setAccount(@WebParam(name = "accountDTO") AccountDTO accountDTO) throws Exception {

		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		accountDTO.setSrvcSeqc(srvcSeqc);		
		
		int result = adminMapper.setAccount(accountDTO);
		return result;
	}

	public List<UserDTO> getUserList(@WebParam(name = "srvcSeqc") int srvcSeqc
			, @WebParam(name = "includeUnusedYN") String includeUnusedYN
			, @WebParam(name = "userId") String userId) throws Exception {
		List<UserDTO> result = null;
		srvcSeqc = (srvcSeqc == 0) ? SessionHelper.getLoginDTO(request).getSrvcSeqc() : srvcSeqc;
		result = metaMapper.getUserList(srvcSeqc, includeUnusedYN, userId);
		return result;
	}

	public UserDTO getUser(
			@WebParam(name = "userSeqc") int userSeqc,
			@WebParam(name = "userId") String userId) throws Exception {
		UserDTO result = metaMapper.getUser(0, userId);
		return result;
	}
	
	public int addUser(@WebParam(name = "userDTO") UserDTO userDTO) throws Exception {
		int result = metaMapper.addUser(userDTO);
		return result;
	}
	
	public int setUser(@WebParam(name = "userDTO") UserDTO userDTO) throws Exception {
		int result = metaMapper.setUser(userDTO);
		return result;
	}

	public ServiceDTO getService(@WebParam(name = "srvcSeqc") int srvcSeqc
			, @WebParam(name = "userId") String userId) throws Exception {
		ServiceDTO result = metaMapper.getService(srvcSeqc, userId);
		return result;
	}
	
	public int setService(@WebParam(name = "serviceDTO") ServiceDTO serviceDTO) throws Exception {
		int result = metaMapper.setService(serviceDTO);
		return result;
	}
	
	public List<SellMntcCostTtalDTO> getSellMntcCostTtal(@WebParam(name = "year") String year) throws Exception {
		List<SellMntcCostTtalDTO> result = null;
		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		result = adminMapper.getSellMntcCostTtal(srvcSeqc, year);
		return result;
	}
	
	public List<SellMntcCostDayTtalDTO> getSellMntcCostDayTtal(@WebParam(name = "yymm") String yymm) throws Exception {
		List<SellMntcCostDayTtalDTO> result = null;
		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		result = adminMapper.getSellMntcCostDayTtal(srvcSeqc, yymm);
		return result;
	}
	
	public List<SellMntcCostDetailDTO> getSellMntcCostDetail(@WebParam(name = "day") String day) throws Exception {
		List<SellMntcCostDetailDTO> result = null;
		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		result = adminMapper.getSellMntcCostDetail(srvcSeqc, day);
		return result;
	}
	
	public int addSellMntcCost(@WebParam(name = "sellMntcCostDetailDTO") SellMntcCostDetailDTO sellMntcCostDetailDTO) throws Exception {
		
		int srvcSeqc = SessionHelper.getLoginDTO(request).getSrvcSeqc();
		sellMntcCostDetailDTO.setSrvcSeqc(srvcSeqc);
		adminMapper.addSellMntcCost(sellMntcCostDetailDTO);		
		int result = sellMntcCostDetailDTO.getSellMntcSeqc();
		return result;
	}
	
	public List<Integer> getBuyPayPerYearList(@WebParam(name = "acntSeqc") int acntSeqc) throws Exception {
		List<Integer> result = adminMapper.getBuyPayPerYearList(acntSeqc);
		return result;
	}
	
//	public List<BuyPayPerMonthDTO> getBuyPayPerMonthList(
//			@WebParam(name = "acntSeqc") int acntSeqc,
//			@WebParam(name = "ocrcYear") int ocrcYear) throws Exception {
//		List<BuyPayPerMonthDTO> result = adminMapper.getBuyPayPerMonthList(acntSeqc, ocrcYear);
//		return result;
//	}
	
}
