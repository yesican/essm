package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="StockListDTO")
public class StockListDTO {
	
	private int prdtMstrSeqc;	//마스터상품순번
	private int prdtSeqc;	//상품순번
	private String prdtNm;	//상품명
	private String clor;	//칼라
	private String size;	//사이즈
	private int mainAcntSeqc;	//주거래처순번
	private String cltsKind;	//품종
	private String frstBuyDay;	//첫사입일
	private int sellPrce; //판매가
	private String acntNm; //거래처명
	private String cltsKindNm; //품종명
	private int ttalBuyQntt;  //입고수량
	private int ttalSellQntt; //판매수량
	private int ttalStckQntt; //재고수량
	private int ttalStckPrce; //재고금액
	
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getFrstBuyDay() {
		return frstBuyDay;
	}
	public void setFrstBuyDay(String frstBuyDay) {
		this.frstBuyDay = frstBuyDay;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public String getAcntNm() {
		return acntNm;
	}
	public void setAcntNm(String acntNm) {
		this.acntNm = acntNm;
	}
	public String getCltsKindNm() {
		return cltsKindNm;
	}
	public void setCltsKindNm(String cltsKindNm) {
		this.cltsKindNm = cltsKindNm;
	}
	public int getTtalBuyQntt() {
		return ttalBuyQntt;
	}
	public void setTtalBuyQntt(int ttalBuyQntt) {
		this.ttalBuyQntt = ttalBuyQntt;
	}
	public int getTtalSellQntt() {
		return ttalSellQntt;
	}
	public void setTtalSellQntt(int ttalSellQntt) {
		this.ttalSellQntt = ttalSellQntt;
	}
	public int getTtalStckQntt() {
		return ttalStckQntt;
	}
	public void setTtalStckQntt(int ttalStckQntt) {
		this.ttalStckQntt = ttalStckQntt;
	}
	public int getTtalStckPrce() {
		return ttalStckPrce;
	}
	public void setTtalStckPrce(int ttalStckPrce) {
		this.ttalStckPrce = ttalStckPrce;
	}
	
	
}
