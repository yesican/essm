package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="MyOptionSizeDTO")
public class MyOptionSizeDTO {

	private int myOptnSeqc;
	private String size;
	private String oldSize;
	
	public int getMyOptnSeqc() {
		return myOptnSeqc;
	}
	public void setMyOptnSeqc(int myOptnSeqc) {
		this.myOptnSeqc = myOptnSeqc;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getOldSize() {
		return oldSize;
	}
	public void setOldSize(String oldSize) {
		this.oldSize = oldSize;
	}
}
