package com.asokorea.essm.common.biz.dto;


import javax.xml.bind.annotation.XmlType;

@XmlType(name = "SellMntcCostTtalDTO")
public class SellMntcCostTtalDTO {

	private String yyyymm;
	private int useMonyTtal;
	
	public String getYyyymm() {
		return yyyymm;
	}
	public void setYyyymm(String yyyymm) {
		this.yyyymm = yyyymm;
	}
	public int getUseMonyTtal() {
		return useMonyTtal;
	}
	public void setUseMonyTtal(int useMonyTtal) {
		this.useMonyTtal = useMonyTtal;
	}
}
