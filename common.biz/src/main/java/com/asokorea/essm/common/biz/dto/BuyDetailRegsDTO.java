package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "BuyDetailRegsDTO")
public class BuyDetailRegsDTO {

	private int buySeqc;		//사입순번
	private int prdtMstrSeqc;	//상품마스터순번
	private int sellPrce;		//판매가
	private int cltsKind;		//품종
	private String yearSesn;	//시즌
	private int buyDtlSeqc;		//사입상세순번
	private int seqc;			//순번
	private String prdtNm;		//상품명
	private String clor;		//색상
	private String size;		//사이즈
	private int prdtSeqc;		//상품순번
	private int buyPrice;		//사입가
	private int buyQntt;		//수량
	private int buyPriceOld;	//수정전 사입가
	private int buyQnttOld;		//수정전 수량
	private String useYn;		//사용여부
	private String regsUserId;	//최초 등록자 아이디
	private Date regsDttm;	//최초 등록 일시
	private String updtUserId;	//최종 수정자 아이디
	private Date updtDttm;	//최종 수정 일시
	
	public int getBuySeqc() {
		return buySeqc;
	}
	public void setBuySeqc(int buySeqc) {
		this.buySeqc = buySeqc;
	}
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public int getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(int cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getYearSesn() {
		return yearSesn;
	}
	public void setYearSesn(String yearSesn) {
		this.yearSesn = yearSesn;
	}
	public int getBuyDtlSeqc() {
		return buyDtlSeqc;
	}
	public void setBuyDtlSeqc(int buyDtlSeqc) {
		this.buyDtlSeqc = buyDtlSeqc;
	}
	public int getSeqc() {
		return seqc;
	}
	public void setSeqc(int seqc) {
		this.seqc = seqc;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public int getBuyPrice() {
		return buyPrice;
	}
	public void setBuyPrice(int buyPrice) {
		this.buyPrice = buyPrice;
	}
	public int getBuyQntt() {
		return buyQntt;
	}
	public void setBuyQntt(int buyQntt) {
		this.buyQntt = buyQntt;
	}
	public int getBuyPriceOld() {
		return buyPriceOld;
	}
	public void setBuyPriceOld(int buyPriceOld) {
		this.buyPriceOld = buyPriceOld;
	}
	public int getBuyQnttOld() {
		return buyQnttOld;
	}
	public void setBuyQnttOld(int buyQnttOld) {
		this.buyQnttOld = buyQnttOld;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
}
