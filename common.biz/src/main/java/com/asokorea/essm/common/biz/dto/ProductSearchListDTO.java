package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ProductSearchListDTO")
public class ProductSearchListDTO {
	
	private int prdtSeqc;		//상품순번
	private String prdtNm;		//상품명
	private String clor;		//칼라
	private String size;		//사이즈
	private String acntNm;		//입고처명
	private String cltsKindNm;	//아이템명
	private String frstbuyDay;	//처사입일
	private int sellPrce;		//판매가
	private String barcode;		//바코드
	
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getAcntNm() {
		return acntNm;
	}
	public void setAcntNm(String acntNm) {
		this.acntNm = acntNm;
	}
	public String getCltsKindNm() {
		return cltsKindNm;
	}
	public void setCltsKindNm(String cltsKindNm) {
		this.cltsKindNm = cltsKindNm;
	}
	public String getFrstbuyDay() {
		return frstbuyDay;
	}
	public void setFrstbuyDay(String frstbuyDay) {
		this.frstbuyDay = frstbuyDay;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
}
