package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import lombok.Data;

@Data
public class UserDTO {

	private int id;
	private int serviceId;
	private String loginId;
	private String loginPassword;
	private String userName;
	private String phone;
	private String email;
	private String role;
	private String note;
	private String delYn;
	private Date lastLoginDttm;
	private String regId;
	private Date regDttm;
	private String updId;
	private Date updDttm;
	private String delId;
	private Date delDttm;

}
