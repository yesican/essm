package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="EventProductSearchDTO")
public class EventProductSearchDTO {
	
	private int evntSeqc;
	private String strtFrstBuyDay;
	private String endFrstBuyDay;
	private int acntSeqc;
	private String prdtNm;
	private String cltsKind;
	private int sortType;
	
	public int getEvntSeqc() {
		return evntSeqc;
	}
	public void setEvntSeqc(int evntSeqc) {
		this.evntSeqc = evntSeqc;
	}
	public String getStrtFrstBuyDay() {
		return strtFrstBuyDay;
	}
	public void setStrtFrstBuyDay(String strtFrstBuyDay) {
		this.strtFrstBuyDay = strtFrstBuyDay;
	}
	public String getEndFrstBuyDay() {
		return endFrstBuyDay;
	}
	public void setEndFrstBuyDay(String endFrstBuyDay) {
		this.endFrstBuyDay = endFrstBuyDay;
	}
	public int getAcntSeqc() {
		return acntSeqc;
	}
	public void setAcntSeqc(int acntSeqc) {
		this.acntSeqc = acntSeqc;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public int getSortType() {
		return sortType;
	}
	public void setSortType(int sortType) {
		this.sortType = sortType;
	}

}
