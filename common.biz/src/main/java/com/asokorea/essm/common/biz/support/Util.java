package com.asokorea.essm.common.biz.support;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.springframework.stereotype.Component;

@Component
public final class Util {

	public static 	int saveProductImage(
			int prdtMstrSeqc,
			String imageSaveDir,
			byte[] imageData) throws Exception {
		
		int result = 0;	// 0:정상처리 1:상품마스터 순번 누락 2:이미지 저장오류 99:서버 오류
		
		try {
			// 이미지 폴더가 없는 경우 생성
			File imgRootPath = new File(imageSaveDir);		
			
			if(!imgRootPath.exists()){
				imgRootPath.mkdirs();
			}
			
			// 상품 아이디가 정상인 경우
			if(prdtMstrSeqc > 0){
				Path path = Paths.get(imageSaveDir, String.valueOf(prdtMstrSeqc));
				Path resultPath = Files.write(path, imageData, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
				
				if(resultPath == null){
					result = 2;
				}
			}else{
	        	result = 1;
	        }
		} catch (Exception e) {
			result = 99;
		}

		return result;
	}
	
	public static 	int deleteProductImage(
			int prdtMstrSeqc,
			String imageSaveDir) throws Exception {
		
		int result = 0; // 0:정상처리 1:상품마스터 순번 누락 2:이미지 삭제오류 99:서버 오류
		
		try {
			// 상품 아이디가 정상인 경우
			if(prdtMstrSeqc > 0){
				Path path = Paths.get(imageSaveDir, String.valueOf(prdtMstrSeqc));
				if(Files.deleteIfExists(path)){
					result = 3;
				}
			}else{
				result = 2;
			}
		} catch (Exception e) {
			result = 99;
		}
		
		return result;
	}

	public static byte[] getProductImage(int prdtMstrSeqc, String imageSaveDir) throws IOException {

		byte[] result = null;
		
		// 상품 아이디가 정상인 경우
		if(prdtMstrSeqc > 0){
			Path path = Paths.get(imageSaveDir, String.valueOf(prdtMstrSeqc));
			if(Files.exists(path, LinkOption.NOFOLLOW_LINKS)){
					result = Files.readAllBytes(path);
			}
		}
		
		return result;
	}
	
}
