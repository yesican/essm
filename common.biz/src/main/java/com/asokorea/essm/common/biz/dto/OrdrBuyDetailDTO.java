package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "OrdrBuyDetailDTO")
public class OrdrBuyDetailDTO {

	private int buySeqc;		//사입순번
	private int seqc;			//순번
	private int prdtSeqc;		//상품순번
	private int buyPrice;		//사입가
	private int buyQntt;		//사입수량
	private String useYn;		//사용여부
	private String regsUserId;	//최초 등록자 아이디
	private Date regsDttm;		//최초 등록 일시
	private String updtUserId;	//최종 수정자 아이디
	private Date updtDttm;		//최종 수정 일시
	private String prdtNm;		//상품명
	private String clor;		//색상
	private String size;		//사이즈
	private String cltsKindNm;	//품종명
	private int ordrQntt;		//주문수량
	
	public int getBuySeqc() {
		return buySeqc;
	}
	public void setBuySeqc(int buySeqc) {
		this.buySeqc = buySeqc;
	}
	public int getSeqc() {
		return seqc;
	}
	public void setSeqc(int seqc) {
		this.seqc = seqc;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public int getBuyPrice() {
		return buyPrice;
	}
	public void setBuyPrice(int buyPrice) {
		this.buyPrice = buyPrice;
	}
	public int getBuyQntt() {
		return buyQntt;
	}
	public void setBuyQntt(int buyQntt) {
		this.buyQntt = buyQntt;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getCltsKindNm() {
		return cltsKindNm;
	}
	public void setCltsKindNm(String cltsKindNm) {
		this.cltsKindNm = cltsKindNm;
	}
	public int getOrdrQntt() {
		return ordrQntt;
	}
	public void setOrdrQntt(int ordrQntt) {
		this.ordrQntt = ordrQntt;
	}
}
