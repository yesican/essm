package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="SellAnalysisDTO")
public class SellAnalysisDTO {
	
	private int prdtMstrSeqc;
	private int prdtSeqc;
	private int mainAcntSeqc;
	private String cltsKind;
	private String prdtNm;
	private String clor;
	private String size;
	private String frstBuyDay;
	private int sellPrce;
	private int ttalBuyQntt;
	private int ttalSellQntt;
	private int ttalStckQntt;
	private int ttalStckPrce;
	private int sellRate;
	private int sellDays;
	private int avrgSellQntt;
	private int ordrQntt;
	private String evntYn;
	
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getFrstBuyDay() {
		return frstBuyDay;
	}
	public void setFrstBuyDay(String frstBuyDay) {
		this.frstBuyDay = frstBuyDay;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public int getTtalBuyQntt() {
		return ttalBuyQntt;
	}
	public void setTtalBuyQntt(int ttalBuyQntt) {
		this.ttalBuyQntt = ttalBuyQntt;
	}
	public int getTtalSellQntt() {
		return ttalSellQntt;
	}
	public void setTtalSellQntt(int ttalSellQntt) {
		this.ttalSellQntt = ttalSellQntt;
	}
	public int getTtalStckQntt() {
		return ttalStckQntt;
	}
	public void setTtalStckQntt(int ttalStckQntt) {
		this.ttalStckQntt = ttalStckQntt;
	}
	public int getTtalStckPrce() {
		return ttalStckPrce;
	}
	public void setTtalStckPrce(int ttalStckPrce) {
		this.ttalStckPrce = ttalStckPrce;
	}
	public int getSellRate() {
		return sellRate;
	}
	public void setSellRate(int sellRate) {
		this.sellRate = sellRate;
	}
	public int getSellDays() {
		return sellDays;
	}
	public void setSellDays(int sellDays) {
		this.sellDays = sellDays;
	}
	public int getAvrgSellQntt() {
		return avrgSellQntt;
	}
	public void setAvrgSellQntt(int avrgSellQntt) {
		this.avrgSellQntt = avrgSellQntt;
	}
	public int getOrdrQntt() {
		return ordrQntt;
	}
	public void setOrdrQntt(int ordrQntt) {
		this.ordrQntt = ordrQntt;
	}
	public String getEvntYn() {
		return evntYn;
	}
	public void setEvntYn(String evntYn) {
		this.evntYn = evntYn;
	}
}
