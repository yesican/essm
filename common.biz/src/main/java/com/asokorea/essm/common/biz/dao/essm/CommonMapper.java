package com.asokorea.essm.common.biz.dao.essm;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.asokorea.essm.common.biz.dto.CommonCodeDTO;
import com.asokorea.essm.common.biz.dto.YearRangeDTO;

@Mapper
public interface CommonMapper {

	public List<YearRangeDTO> getYearRange(@Param("srvcSeqc") int srvcSeqc) throws Exception;
	
	public List<CommonCodeDTO> getCommonCodeDetail() throws Exception;
	
	@Select("select NOW()")
    public Date getDBDate();
}