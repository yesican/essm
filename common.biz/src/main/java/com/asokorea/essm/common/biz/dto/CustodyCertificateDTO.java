package com.asokorea.essm.common.biz.dto;


import javax.xml.bind.annotation.XmlType;

@XmlType(name = "CustodyCertificateDTO")
public class CustodyCertificateDTO {
	private int cstdCtfcSeqc;		//보관증순번
	private String pbshDt;			//보관증발행일자
	private String cstdCtfcNo;		//보관증발행번호
	private int markMony;			//보관증발행액
	private String cltnDay;			//보관증회수일자
	private String useExpnDay;		//보관증만료일자
	
	public int getCstdCtfcSeqc() {
		return cstdCtfcSeqc;
	}
	public void setCstdCtfcSeqc(int cstdCtfcSeqc) {
		this.cstdCtfcSeqc = cstdCtfcSeqc;
	}
	public String getPbshDt() {
		return pbshDt;
	}
	public void setPbshDt(String pbshDt) {
		this.pbshDt = pbshDt;
	}
	public String getCstdCtfcNo() {
		return cstdCtfcNo;
	}
	public void setCstdCtfcNo(String cstdCtfcNo) {
		this.cstdCtfcNo = cstdCtfcNo;
	}
	public int getMarkMony() {
		return markMony;
	}
	public void setMarkMony(int markMony) {
		this.markMony = markMony;
	}
	public String getCltnDay() {
		return cltnDay;
	}
	public void setCltnDay(String cltnDay) {
		this.cltnDay = cltnDay;
	}
	public String getUseExpnDay() {
		return useExpnDay;
	}
	public void setUseExpnDay(String useExpnDay) {
		this.useExpnDay = useExpnDay;
	}
}
