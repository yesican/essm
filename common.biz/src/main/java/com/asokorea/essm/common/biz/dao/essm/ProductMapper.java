package com.asokorea.essm.common.biz.dao.essm;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.asokorea.essm.common.biz.dto.EventDTO;
import com.asokorea.essm.common.biz.dto.EventProductDTO;
import com.asokorea.essm.common.biz.dto.EventProductSearchDTO;
import com.asokorea.essm.common.biz.dto.MyOptionClorDTO;
import com.asokorea.essm.common.biz.dto.MyOptionDTO;
import com.asokorea.essm.common.biz.dto.MyOptionDetailDTO;
import com.asokorea.essm.common.biz.dto.MyOptionSizeDTO;
import com.asokorea.essm.common.biz.dto.ProductDetailsRegsDTO;
import com.asokorea.essm.common.biz.dto.ProductRegsColDTO;
import com.asokorea.essm.common.biz.dto.ProductRegsDTO;
import com.asokorea.essm.common.biz.dto.ProductRegsSizeDTO;
import com.asokorea.essm.common.biz.dto.ProductSearchListDTO;

@Mapper
public interface ProductMapper {

	public int addEvent(@Param("eventDTO") EventDTO eventDTO) throws Exception;

	public int addEventProduct(@Param("eventProductDTO") EventProductDTO eventProductDTO) throws Exception;
	
	public List<EventDTO> getEvent(@Param("evntNm") String evntNm, @Param("includeEndEventYN") String includeEndEventYN) throws Exception;

	public List<EventProductDTO> getEventProduct(@Param("evntSeqc") int evntSeqc) throws Exception;

	public List<EventProductDTO> getUnselectEventProduct(@Param("eventProductSearchDTO") EventProductSearchDTO eventProductSearchDTO) throws Exception;
	
	public int delEventProduct(@Param("evntSeqc") int evntSeqc, @Param("prdtMstrSeqc") int prdtMstrSeqc) throws Exception;

	public int setEvent(@Param("eventDTO") EventDTO eventDTO) throws Exception;

	public int setEventProduct(@Param("eventDTO") EventDTO eventDTO, @Param("eventProductDTO") EventProductDTO eventProductDTO) throws Exception;
	
	public List<ProductRegsDTO> getProductRegList(@Param("startDate") String startDate, @Param("endDate") String endDate, @Param("mainAcntSeqc") int mainAcntSeqc, 
			@Param("srvcSeqc") int acntSeqc, @Param("prdtNm") String prdtNm, @Param("cltsKind") String cltsKind, @Param("sellSttsYN") String sellSttsYN) throws Exception;
	
	public List<ProductRegsColDTO> productRegsGetColList(@Param("prdtMstrSeqc") int prdtMstrSeqc) throws Exception;
	
	public List<ProductRegsSizeDTO> productRegsGetSizeList(@Param("prdtMstrSeqc") int prdtMstrSeqc) throws Exception;
	
	public int addProduct(@Param("productRegsDTO") ProductRegsDTO productRegsDTO) throws Exception;
	
	public int setProduct(@Param("productRegsDTO") ProductRegsDTO productRegsDTO) throws Exception;
   
	public int setProductRegsDetail(@Param("productDetailsRegsDTO") ProductDetailsRegsDTO productDetailsRegsDTO) throws Exception;
	
	public int addProductRegsDetail(@Param("productDetailsRegsDTO") ProductDetailsRegsDTO productDetailsRegsDTO) throws Exception;
	
	public List<ProductRegsColDTO> productRegsGetNewColList() throws Exception;
	
	public List<ProductRegsSizeDTO> productRegsGetNewSizeList() throws Exception;
	
	public List<MyOptionDTO> getMyOtion (@Param("srvcSeqc") int srvcSeqc) throws Exception;
	
	public List<MyOptionClorDTO> getMyOtionCol (@Param("myOptnSeqc") int myOptnSeqc) throws Exception;
	
	public List<MyOptionSizeDTO> getMyOtionSize (@Param("myOptnSeqc") int myOptnSeqc) throws Exception;
	
	public int addMyOtion(@Param("myOptionDTO") MyOptionDTO myOptionDTO) throws Exception;
	
	public int setMyOtion(@Param("myOptionDTO") MyOptionDTO myOptionDTO) throws Exception;
	
	public int delMyOtion(@Param("myOptnSeqc") int myOptnSeqc) throws Exception;
	
	public int addMyOtionDetail(@Param("myOptionDetailDTO") MyOptionDetailDTO myOptionDetailDTO, @Param("myOptnSeqc") int myOptnSeqc) throws Exception;
	
	public int delMyOtionDetail(@Param("myOptnSeqc") int myOptnSeqc) throws Exception;
	
	public List<ProductSearchListDTO> getProductSearchList (
			@Param("srvcSeqc") int srvcSeqc,
			@Param("barcode") String barcode,
			@Param("prdtNm") String prdtNm) throws Exception;
	
}