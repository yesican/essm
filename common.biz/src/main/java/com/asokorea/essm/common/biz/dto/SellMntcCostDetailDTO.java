package com.asokorea.essm.common.biz.dto;

import java.util.Date;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "SellMntcCostDetailDTO")
public class SellMntcCostDetailDTO {

	private int sellMntcSeqc;
	private int srvcSeqc;
	private String orcrDt;
	private int useMony;
	private String cntt;
	private String note;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getSellMntcSeqc() {
		return sellMntcSeqc;
	}
	public void setSellMntcSeqc(int sellMntcSeqc) {
		this.sellMntcSeqc = sellMntcSeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getOrcrDt() {
		return orcrDt;
	}
	public void setOrcrDt(String orcrDt) {
		this.orcrDt = orcrDt;
	}
	public int getUseMony() {
		return useMony;
	}
	public void setUseMony(int useMony) {
		this.useMony = useMony;
	}
	public String getCntt() {
		return cntt;
	}
	public void setCntt(String cntt) {
		this.cntt = cntt;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
	
}
