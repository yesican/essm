package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "CommonCodeDTO")
public class CommonCodeDTO {

	private String cmmnCdId;
	private String cmmnCdNm;
	private String dtlCdId;
	private String dtlCdNm;
	private String bynm;
	private int arrySeqc;

	public String getCmmnCdId() {
		return cmmnCdId;
	}

	public void setCmmnCdId(String cmmnCdId) {
		this.cmmnCdId = cmmnCdId;
	}

	public String getCmmnCdNm() {
		return cmmnCdNm;
	}

	public void setCmmnCdNm(String cmmnCdNm) {
		this.cmmnCdNm = cmmnCdNm;
	}
	
	public String getDtlCdId() {
		return dtlCdId;
	}

	public void setDtlCdId(String dtlCdId) {
		this.dtlCdId = dtlCdId;
	}

	public String getDtlCdNm() {
		return dtlCdNm;
	}

	public void setDtlCdNm(String dtlCdNm) {
		this.dtlCdNm = dtlCdNm;
	}

	public String getBynm() {
		return bynm;
	}

	public void setBynm(String bynm) {
		this.bynm = bynm;
	}

	public int getArrySeqc() {
		return arrySeqc;
	}

	public void setArrySeqc(int arrySeqc) {
		this.arrySeqc = arrySeqc;
	}

}
