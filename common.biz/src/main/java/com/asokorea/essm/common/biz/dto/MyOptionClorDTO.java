package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="MyOptionClorDTO")
public class MyOptionClorDTO {

	private int myOptnSeqc;
	private String clor;
	private String oldClor;
	
	public int getMyOptnSeqc() {
		return myOptnSeqc;
	}
	public void setMyOptnSeqc(int myOptnSeqc) {
		this.myOptnSeqc = myOptnSeqc;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getOldClor() {
		return oldClor;
	}
	public void setOldClor(String oldClor) {
		this.oldClor = oldClor;
	}
	
}
