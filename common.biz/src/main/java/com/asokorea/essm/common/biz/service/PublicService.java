package com.asokorea.essm.common.biz.service;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asokorea.essm.common.biz.dao.meta.MetaMapper;
import com.asokorea.essm.common.biz.dto.LoginDTO;
import com.asokorea.essm.common.biz.dto.ServiceDTO;
import com.asokorea.essm.common.biz.dto.UserDTO;
import com.asokorea.essm.common.biz.support.SessionHelper;

@Service
public class PublicService {

	private final static Logger logger = LoggerFactory.getLogger(PublicService.class);
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private MetaMapper metaMapper;
	
//	@Autowired
//	@Qualifier("primaryTxManager")
//	private DataSourceTransactionManager primaryTxManager;	
//	
//	@Autowired
//	@Qualifier("metaTxManager")
//	private DataSourceTransactionManager metaTxManager;	
	
	public int login(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "userPwd") String userPwd,
			@WebParam(name = "loginMode") String loginMode) throws Exception {
		
		// 로그인 결과 0:정상, 1:아이디 없음, 2:패스워드 틀림, 3:사용불가(미결제, 휴면, 불량)
		int result = 0;
		HttpSession session = SessionHelper.getSession(request);
		LoginDTO loginDTO = new LoginDTO();
		ServiceDTO serviceDTO = null;
		UserDTO user = null;
		int srvcSeqc = 0;

		serviceDTO = metaMapper.getService(srvcSeqc, userId);
		
		if(serviceDTO != null){
			srvcSeqc = serviceDTO.getSrvcSeqc();
			user = metaMapper.getUser(0, userId);
			
			if(userPwd.equals(user.getLoginPassword())){
				
				// TODO : 로그인 이후 업데이트 할거
				loginDTO.setUserDTO(user);
				loginDTO.setServiceDTO(serviceDTO);				
				loginDTO.setLoginResult(0);	// 정상 로그인
				SessionHelper.setLoginDTO(loginDTO, request);
			}else{
				loginDTO.setLoginResult(2);	// 패스워드 틀림
				result = 2;
			}
		}else{
			loginDTO.setLoginResult(1);	// 아이디 없음
			result = 1;
		}

		String sessionUserId = (loginDTO != null) ? loginDTO.getUserId() : null;
		int sessionServiceSeq = (loginDTO != null) ? loginDTO.getSrvcSeqc() : 0;
		
		logger.info("############################ SET SESSION ################################");		
		logger.info("########## SESSION ID : " + session.getId());
		logger.info("########## 서비스 순번 : " + sessionServiceSeq);
		logger.info("########## 사용자 아이디 : " + sessionUserId);
		logger.info("############################ SET SESSION ################################");		
		
		return result;
	}
	
	public void logout() throws Exception {
		HttpSession session = SessionHelper.getSession(request);
		session.invalidate();
	}

}
