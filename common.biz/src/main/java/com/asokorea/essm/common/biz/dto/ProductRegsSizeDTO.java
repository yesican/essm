package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ProductRegsSizeDTO")
public class ProductRegsSizeDTO {

	private int prdtMstrSeqc;
	private String size;
	private String oldSize;
	
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getOldSize() {
		return oldSize;
	}
	public void setOldSize(String oldSize) {
		this.oldSize = oldSize;
	}
}
