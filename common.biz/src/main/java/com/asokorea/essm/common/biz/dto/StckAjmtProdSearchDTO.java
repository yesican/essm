package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="StckAjmtProdSearchDTO")
public class StckAjmtProdSearchDTO {
	
	private int srvcSeqc;			//서비스순번
	private String startFrstBuyDay;	//첫사입일시작일
	private String endFrstBuyDay;	//첫사입일시작일
	private int acntSeqc;			//거래처순번
	private int cltsKind;		//품종
	private String prdtNm; 			//상품명
	private int sortType;			//정렬타입
	
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getStartFrstBuyDay() {
		return startFrstBuyDay;
	}
	public void setStartFrstBuyDay(String startFrstBuyDay) {
		this.startFrstBuyDay = startFrstBuyDay;
	}
	public String getEndFrstBuyDay() {
		return endFrstBuyDay;
	}
	public void setEndFrstBuyDay(String endFrstBuyDay) {
		this.endFrstBuyDay = endFrstBuyDay;
	}
	public int getAcntSeqc() {
		return acntSeqc;
	}
	public void setAcntSeqc(int acntSeqc) {
		this.acntSeqc = acntSeqc;
	}
	public int getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(int cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public int getSortType() {
		return sortType;
	}
	public void setSortType(int sortType) {
		this.sortType = sortType;
	}
}
