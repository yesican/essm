package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "StckAjmtSellDTO")
public class StckAjmtSellDTO {

	private int sellSeqc;		//판매순번
	private int srvcSeqc;		//서비스순번
	private String sellDt;		//판매일시
	private String sellDvsn;	//판매구분
	private int ctmrSeqc;		//고객순번
	private String receiptNo;	//영수증번호
	private int ttalSellMony;	//총판매액
	private int addDcntMony;	//추가할인액
	private int pintUseMony;	//포인트사용액
	private int cstdCtfcUseMony;//보관증사용액
	private int crdtMony;		//외상액
	private int paymMony;		//결제금액
	private String cutrNum;		//계산대번호
	private String slerId;		//판매자ID
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getSellSeqc() {
		return sellSeqc;
	}
	public void setSellSeqc(int sellSeqc) {
		this.sellSeqc = sellSeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getSellDt() {
		return sellDt;
	}
	public void setSellDt(String sellDt) {
		this.sellDt = sellDt;
	}
	public String getSellDvsn() {
		return sellDvsn;
	}
	public void setSellDvsn(String sellDvsn) {
		this.sellDvsn = sellDvsn;
	}
	public int getCtmrSeqc() {
		return ctmrSeqc;
	}
	public void setCtmrSeqc(int ctmrSeqc) {
		this.ctmrSeqc = ctmrSeqc;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public int getTtalSellMony() {
		return ttalSellMony;
	}
	public void setTtalSellMony(int ttalSellMony) {
		this.ttalSellMony = ttalSellMony;
	}
	public int getAddDcntMony() {
		return addDcntMony;
	}
	public void setAddDcntMony(int addDcntMony) {
		this.addDcntMony = addDcntMony;
	}
	public int getPintUseMony() {
		return pintUseMony;
	}
	public void setPintUseMony(int pintUseMony) {
		this.pintUseMony = pintUseMony;
	}
	public int getCstdCtfcUseMony() {
		return cstdCtfcUseMony;
	}
	public void setCstdCtfcUseMony(int cstdCtfcUseMony) {
		this.cstdCtfcUseMony = cstdCtfcUseMony;
	}
	public int getCrdtMony() {
		return crdtMony;
	}
	public void setCrdtMony(int crdtMony) {
		this.crdtMony = crdtMony;
	}
	public int getPaymMony() {
		return paymMony;
	}
	public void setPaymMony(int paymMony) {
		this.paymMony = paymMony;
	}
	public String getCutrNum() {
		return cutrNum;
	}
	public void setCutrNum(String cutrNum) {
		this.cutrNum = cutrNum;
	}
	public String getSlerId() {
		return slerId;
	}
	public void setSlerId(String slerId) {
		this.slerId = slerId;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
}
