package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="BasketLisDTO")
public class BasketLisDTO {

	private int srvcSeqc;
	private int prdtSeqc;
	private String prdtNm;
	private String clor;
	private String size;
	private int mainAcntSeqc;
	private String cltsKind;
	private String frstBuyDay;
	private int ttalStckQntt;
	private int ordrQntt;
	private String useYn;
	private String regsUserId;
	private String updtUserId;
	
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getFrstBuyDay() {
		return frstBuyDay;
	}
	public void setFrstBuyDay(String frstBuyDay) {
		this.frstBuyDay = frstBuyDay;
	}
	public int getTtalStckQntt() {
		return ttalStckQntt;
	}
	public void setTtalStckQntt(int ttalStckQntt) {
		this.ttalStckQntt = ttalStckQntt;
	}
	public int getOrdrQntt() {
		return ordrQntt;
	}
	public void setOrdrQntt(int ordrQntt) {
		this.ordrQntt = ordrQntt;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	
}
