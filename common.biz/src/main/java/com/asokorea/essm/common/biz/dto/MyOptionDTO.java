package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="MyOptionDTO")
public class MyOptionDTO {

	private int myOptnSeqc;
	private int srvcSeqc;
	private String optnNm;
	private String chicYn;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getMyOptnSeqc() {
		return myOptnSeqc;
	}
	public void setMyOptnSeqc(int myOptnSeqc) {
		this.myOptnSeqc = myOptnSeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getOptnNm() {
		return optnNm;
	}
	public void setOptnNm(String optnNm) {
		this.optnNm = optnNm;
	}
	public String getChicYn() {
		return chicYn;
	}
	public void setChicYn(String chicYn) {
		this.chicYn = chicYn;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
}
