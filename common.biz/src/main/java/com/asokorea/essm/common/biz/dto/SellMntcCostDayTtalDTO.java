package com.asokorea.essm.common.biz.dto;


import javax.xml.bind.annotation.XmlType;

@XmlType(name = "SellMntcCostDayTtalDTO")
public class SellMntcCostDayTtalDTO {

	private String Date;
	private String mmdd;
	private String dayNm;
	private int useMonyTtal;
	
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getMmdd() {
		return mmdd;
	}
	public void setMmdd(String mmdd) {
		this.mmdd = mmdd;
	}
	public String getDayNm() {
		return dayNm;
	}
	public void setDayNm(String dayNm) {
		this.dayNm = dayNm;
	}
	public int getUseMonyTtal() {
		return useMonyTtal;
	}
	public void setUseMonyTtal(int useMonyTtal) {
		this.useMonyTtal = useMonyTtal;
	}
}
