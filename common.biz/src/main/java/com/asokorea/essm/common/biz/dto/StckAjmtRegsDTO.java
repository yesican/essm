package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "StckAjmtRegsDTO")
public class StckAjmtRegsDTO {

	private int stckAjmtSeqc;
	private int srvcSeqc;
	private String stckAjmtDay;
	private int stckAjmtTtal;
	private String note;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getStckAjmtSeqc() {
		return stckAjmtSeqc;
	}
	public void setStckAjmtSeqc(int stckAjmtSeqc) {
		this.stckAjmtSeqc = stckAjmtSeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getStckAjmtDay() {
		return stckAjmtDay;
	}
	public void setStckAjmtDay(String stckAjmtDay) {
		this.stckAjmtDay = stckAjmtDay;
	}
	public int getStckAjmtTtal() {
		return stckAjmtTtal;
	}
	public void setStckAjmtTtal(int stckAjmtTtal) {
		this.stckAjmtTtal = stckAjmtTtal;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
}
