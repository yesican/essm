package com.asokorea.essm.common.biz.support;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertyHelper {

	@Value("${system.version}")
    public static String systemVersion;	

	@Value("${image.root.path}")
    public static String imageRootPath;	

	@Value("${image.temp.path}")
    public static String imageTempPath;	

	@Value("${summary.prevdays}")
	public static int summayPrevDays;
	
}
