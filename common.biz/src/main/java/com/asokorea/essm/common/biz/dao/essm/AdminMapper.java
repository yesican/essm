package com.asokorea.essm.common.biz.dao.essm;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.asokorea.essm.common.biz.dto.AccountDTO;
import com.asokorea.essm.common.biz.dto.CounterDTO;
import com.asokorea.essm.common.biz.dto.SellMntcCostDayTtalDTO;
import com.asokorea.essm.common.biz.dto.SellMntcCostDetailDTO;
import com.asokorea.essm.common.biz.dto.SellMntcCostTtalDTO;

@Mapper
public interface AdminMapper {
	
	public Date getDBDate() throws Exception;
	
	public List<AccountDTO> getAccountList(
			@Param("srvcSeqc") int srvcSeqc,
			@Param("acntNm") String acntNm,
			@Param("acntDvsn") String acntDvsn,
			@Param("includeEndYN") String includeEndYN) throws Exception;
	
	public int addAccount(Map<String, Object> map) throws Exception;

	public int setAccount(@Param("accountDTO") AccountDTO accountDTO) throws Exception;
	
	public List<SellMntcCostTtalDTO> getSellMntcCostTtal(
			@Param("srvcSeqc") int srvcSeqc,
			@Param("year") String year) throws Exception;
	
	public List<SellMntcCostDayTtalDTO> getSellMntcCostDayTtal(
			@Param("srvcSeqc") int srvcSeqc,
			@Param("yymm") String year) throws Exception;
	
	public List<SellMntcCostDetailDTO> getSellMntcCostDetail(
			@Param("srvcSeqc") int srvcSeqc,
			@Param("day") String day) throws Exception;
	
	public int addSellMntcCost(@Param("sellMntcCostDetailDTO") SellMntcCostDetailDTO sellMntcCostDetailDTO) throws Exception;

	public CounterDTO getCounter(Map<String, Object> map) throws Exception;

	public List<Integer> getBuyPayPerYearList(@Param("acntSeqc") int acntSeqc) throws Exception;

//	public List<BuyPayPerMonthDTO> getBuyPayPerMonthList(@Param("acntSeqc")int acntSeqc,@Param("ocrcYear") int ocrcYear) throws Exception;
}