package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="OrderBuyMainDTO")
public class OrderBuyMainDTO {
	
	private String ordrDay;
	private int mainAcntSeqc;
	private int ordrQntt;
	private String refcCntt;
	private String acntNm;
	
	public String getOrdrDay() {
		return ordrDay;
	}
	public void setOrdrDay(String ordrDay) {
		this.ordrDay = ordrDay;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public int getOrdrQntt() {
		return ordrQntt;
	}
	public void setOrdrQntt(int ordrQntt) {
		this.ordrQntt = ordrQntt;
	}
	public String getRefcCntt() {
		return refcCntt;
	}
	public void setRefcCntt(String refcCntt) {
		this.refcCntt = refcCntt;
	}
	public String getAcntNm() {
		return acntNm;
	}
	public void setAcntNm(String acntNm) {
		this.acntNm = acntNm;
	}
}
