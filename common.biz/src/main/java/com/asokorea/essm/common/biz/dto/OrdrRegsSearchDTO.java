package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "OrdrRegsSearchDTO")
public class OrdrRegsSearchDTO {

	private int srvcSeqc;
	private String strtFrstBuyDay;
	private String endFrstBuyDay;
	private int mainAcntSeqc;
	private String prdtNm;
	private int cltsKind;
	private int sortType;
	
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getStrtFrstBuyDay() {
		return strtFrstBuyDay;
	}
	public void setStrtFrstBuyDay(String strtFrstBuyDay) {
		this.strtFrstBuyDay = strtFrstBuyDay;
	}
	public String getEndFrstBuyDay() {
		return endFrstBuyDay;
	}
	public void setEndFrstBuyDay(String endFrstBuyDay) {
		this.endFrstBuyDay = endFrstBuyDay;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public int getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(int cltsKind) {
		this.cltsKind = cltsKind;
	}
	public int getSortType() {
		return sortType;
	}
	public void setSortType(int sortType) {
		this.sortType = sortType;
	}

}
