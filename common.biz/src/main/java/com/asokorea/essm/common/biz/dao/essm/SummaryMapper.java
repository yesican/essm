package com.asokorea.essm.common.biz.dao.essm;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SummaryMapper {

	public int truncateSummayProduct() throws Exception;
	
	public int sumProduct(@Param("summayPrevDays") int summayPrevDays) throws Exception;
  
}