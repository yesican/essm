package com.asokorea.essm.common.biz.service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.List;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlSchemaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import com.asokorea.essm.common.biz.dao.essm.CommonMapper;
import com.asokorea.essm.common.biz.dto.CommonCodeDTO;
import com.asokorea.essm.common.biz.dto.YearRangeDTO;
import com.asokorea.essm.common.biz.support.PropertyHelper;
import com.asokorea.essm.common.biz.support.SessionHelper;

@Service
public class CommonService {

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private CommonMapper commonMapper;

	public List<YearRangeDTO> getYearRange(@WebParam(name = "srvcSeqc") int srvcSeqc) throws Exception {
		List<YearRangeDTO> list = null;
		list = commonMapper.getYearRange(srvcSeqc);
		return list;
	}
	
	public List<CommonCodeDTO> getCommonCodeDetail() throws Exception {
		List<CommonCodeDTO> list = null;
		list = commonMapper.getCommonCodeDetail();
		return list;
	}
	
	@XmlSchemaType(name = "date")
	public Date getDBDate() throws Exception {
		Date date = commonMapper.getDBDate();
		return date;
	}
	
	public int setProductImage(
			@WebParam(name = "prdtMstrSeqc") int prdtMstrSeqc,
			@WebParam(name = "delImageYn") String delImageYn,
			@WebParam(name = "data") byte[] data,
			@WebParam(name = "imageFileName") String imageFileName,
			@WebParam(name = "imageFileExt") String imageFileExt) throws Exception {
		
		// 이미지 폴더가 없는 경우 생성
		File imgRootPath = new File(PropertyHelper.imageRootPath);		
		
		if(!imgRootPath.exists()){
			imgRootPath.mkdirs();
		}
		
		// 상품 아이디가 정상인 경우
		if(prdtMstrSeqc > 0){
			int srvcSeqc = SessionHelper.getServieSeqc(request);
			String fileName = prdtMstrSeqc + "." + imageFileExt;
			Path path = Paths.get(PropertyHelper.imageRootPath, String.valueOf(srvcSeqc), fileName);

			if(delImageYn.equals("Y")){
				Files.deleteIfExists(path);
			}else if(data != null && data.length > 0 && imageFileName != null && imageFileName != null){
				path = Files.write(path, data, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
			}else{
	        	throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "잘못된 이미지 정보 입니다.");
	        }
		}else{
        	throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "상품 ID가 없습니다.");
        }

		return 0;
	}
}
