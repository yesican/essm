package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="CstdCtfcDTO")
public class CstdCtfcDTO {
	
	private int cstdCtfcSeqc;
	private int srvcSeqc;
	private int markMony;
	private String pbshDt;
	private String cltnDay;
	private String useExpnDay;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getCstdCtfcSeqc() {
		return cstdCtfcSeqc;
	}
	public void setCstdCtfcSeqc(int cstdCtfcSeqc) {
		this.cstdCtfcSeqc = cstdCtfcSeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public int getMarkMony() {
		return markMony;
	}
	public void setMarkMony(int markMony) {
		this.markMony = markMony;
	}
	public String getPbshDt() {
		return pbshDt;
	}
	public void setPbshDt(String pbshDt) {
		this.pbshDt = pbshDt;
	}
	public String getCltnDay() {
		return cltnDay;
	}
	public void setCltnDay(String cltnDay) {
		this.cltnDay = cltnDay;
	}
	public String getUseExpnDay() {
		return useExpnDay;
	}
	public void setUseExpnDay(String useExpnDay) {
		this.useExpnDay = useExpnDay;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
}
