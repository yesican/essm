package com.asokorea.essm.common.biz.dao.essm;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.asokorea.essm.common.biz.dto.CreditDTO;
import com.asokorea.essm.common.biz.dto.CustomerDTO;
import com.asokorea.essm.common.biz.dto.PointDTO;
import com.asokorea.essm.common.biz.dto.RepairDTO;

@Mapper
public interface CustomerMapper {

	public List<CustomerDTO> getCustomerList(
			@Param("customerDTO") CustomerDTO customerDTO,
			@Param("month") String month,
			@Param("day") String day
			) throws Exception;

	public int addCustomer(Map<String, Object> map) throws Exception;
	
	public int setCustomer(@Param("customerDTO") CustomerDTO customerDTO) throws Exception;

	public CustomerDTO getCustomer(@Param("ctmrSeqc") int ctmrSeqc) throws Exception;
	
	public List<PointDTO> getPointUsageList(@Param("ctmrSeqc") int ctmrSeqc) throws Exception;

	public List<CreditDTO> getCreditList(@Param("ctmrSeqc") int ctmrSeqc) throws Exception;

	public List<RepairDTO> getRepairList(Map<String, Object> map) throws Exception;

	public int addRepair(Map<String, Object> map) throws Exception;

	public int setRepair(Map<String, Object> map) throws Exception;

	public CustomerDTO getCustomerStat(Map<String, Object> map) throws Exception;

	public int setCustomerStat(Map<String, Object> map) throws Exception;
	
	public int addUsePoint(Map<String, Object> map) throws Exception;

	public int addUseCstdCtfc(Map<String, Object> map) throws Exception;;

	public int addUseCredit(Map<String, Object> map) throws Exception;
	
	public int addRepayCredit(Map<String, Object> map) throws Exception;
	
	public int delCustomerCredit(Map<String, Object> map) throws Exception;

}
