package com.asokorea.essm.common.biz.service;

import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asokorea.essm.common.biz.dao.essm.CustomerMapper;
import com.asokorea.essm.common.biz.dto.CreditDTO;
import com.asokorea.essm.common.biz.dto.CustomerDTO;
import com.asokorea.essm.common.biz.dto.PointDTO;
import com.asokorea.essm.common.biz.dto.RepairDTO;
import com.asokorea.essm.common.biz.support.SessionHelper;

@Service
public class CustomerService {
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private CustomerMapper customerMapper;
	
	public List<CustomerDTO> getCustomerList(
			@WebParam(name = "customerDTO") CustomerDTO customerDTO,
			@WebParam(name = "month") String month,
			@WebParam(name = "day") String day
			) throws Exception {
		List<CustomerDTO> list = null;
		list = customerMapper.getCustomerList(customerDTO, month, day);
		return list;
	}

	public int addCustomer(@WebParam(name = "customerDTO") CustomerDTO customerDTO) throws Exception {
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("customerDTO", customerDTO);
		customerMapper.addCustomer(map);		
		int result = customerDTO.getCtmrSeqc();
		return result;
	}
	
	public int setCustomer(@WebParam(name = "customerDTO") CustomerDTO customerDTO) throws Exception {
		int result = customerMapper.setCustomer(customerDTO);		
		return result;
	}
	
	public CustomerDTO getCustomer(@WebParam(name = "ctmrSeqc") int ctmrSeqc) throws Exception {
		CustomerDTO result = customerMapper.getCustomer(ctmrSeqc);		
		return result;
	}
	
	public List<PointDTO> getPointUsageList(@WebParam(name = "ctmrSeqc") int ctmrSeqc) throws Exception {
		List<PointDTO> list = null;
		list = customerMapper.getPointUsageList(ctmrSeqc);
		return list;
	}	
	
	public List<CreditDTO> getCreditList(@WebParam(name = "ctmrSeqc") int ctmrSeqc) throws Exception {
		List<CreditDTO> list = null;
		list = customerMapper.getCreditList(ctmrSeqc);
		return list;
	}	
	
	public List<RepairDTO> getRepairList(
			@WebParam(name = "rectDateStart") String rectDateStart,
			@WebParam(name = "rectDateEnd") String rectDateEnd,
			@WebParam(name = "ctmrNm") String ctmrNm,
			@WebParam(name = "repairState") String repairState,
			@WebParam(name = "includeReturnYn") String includeReturnYn
			) throws Exception {
		List<RepairDTO> list = null;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("rectDateStart", rectDateStart);
		map.put("rectDateEnd", rectDateEnd);
		map.put("ctmrNm", ctmrNm);
		map.put("repairState", repairState);
		map.put("includeReturnYn", includeReturnYn);
		list = customerMapper.getRepairList(map);
		return list;
	}	

	public int addRepair(@WebParam(name = "repairDTO") RepairDTO repairDTO) throws Exception {
		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("repairDTO", repairDTO);
		result = customerMapper.addRepair(map);
		return result;
	}
	
	public int setRepair(@WebParam(name = "repairDTO") RepairDTO repairDTO) throws Exception {
		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("repairDTO", repairDTO);
		result = customerMapper.setRepair(map);
		return result;
	}
	
	public int addRepayCredit(@WebParam(name = "ctmrSeqc") int ctmrSeqc,
			@WebParam(name = "mony") int mony,
			@WebParam(name = "crdtRpayNote") String crdtRpayNote) throws Exception {
		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("ctmrSeqc", ctmrSeqc);
		map.put("mony", mony);
		map.put("crdtRpayNote", crdtRpayNote);
		map.put("ctmrCrdtSeqc", 0);	// 생성키 리턴용 아웃풋 파라미터
		customerMapper.addRepayCredit(map);
		result = (int) map.get("ctmrCrdtSeqc");
		return result;
	}
	
	public int delCustomerCredit(@WebParam(name = "ctmrCrdtSeqc") int ctmrCrdtSeqc) throws Exception {
		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("ctmrCrdtSeqc", ctmrCrdtSeqc);
		result = customerMapper.delCustomerCredit(map);
		return result;
	}
	
}
