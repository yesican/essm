package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "BuyRegsDTO")
public class BuyRegsDTO {

	private int buySeqc;		//사입순번
	private int srvcSeqc;		//서비스순번
	private String buyDay;		//사입일자
	private String buyDvsn;		//사입구분
	private int acntSeqc;		//거래처ID
	private int buyTtal;		//*사입총수량
	private String useYn;		//사용여부
	private String regsUserId;	//최초 등록자 아이디
	private Date regsDttm;		//최초 등록 일시
	private String updtUserId;	//최종 수정자 아이디
	private Date updtDttm;		//최종 수정 일시
	
	public int getBuySeqc() {
		return buySeqc;
	}
	public void setBuySeqc(int buySeqc) {
		this.buySeqc = buySeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getBuyDay() {
		return buyDay;
	}
	public void setBuyDay(String buyDay) {
		this.buyDay = buyDay;
	}
	public String getBuyDvsn() {
		return buyDvsn;
	}
	public void setBuyDvsn(String buyDvsn) {
		this.buyDvsn = buyDvsn;
	}
	public int getAcntSeqc() {
		return acntSeqc;
	}
	public void setAcntSeqc(int acntSeqc) {
		this.acntSeqc = acntSeqc;
	}
	public int getBuyTtal() {
		return buyTtal;
	}
	public void setBuyTtal(int buyTtal) {
		this.buyTtal = buyTtal;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
}
