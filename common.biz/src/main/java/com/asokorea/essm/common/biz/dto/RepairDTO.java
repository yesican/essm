package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "RepairDTO")
public class RepairDTO {

	private int reprSeqc;
	private int ctmrSeqc;
	private String ctmrNm;
	private String state;
	private String rectDate;
	private String reprDate;
	private String cmptDate;
	private String returnDate;
	private String prdtNm;
	private int reprAcntSeqc;
	private String acntNm;
	private String rqst;
	private String phone;
	private String notiYn;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	public int getReprSeqc() {
		return reprSeqc;
	}
	public void setReprSeqc(int reprSeqc) {
		this.reprSeqc = reprSeqc;
	}
	public int getCtmrSeqc() {
		return ctmrSeqc;
	}
	public void setCtmrSeqc(int ctmrSeqc) {
		this.ctmrSeqc = ctmrSeqc;
	}
	public String getCtmrNm() {
		return ctmrNm;
	}
	public void setCtmrNm(String ctmrNm) {
		this.ctmrNm = ctmrNm;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getRectDate() {
		return rectDate;
	}
	public void setRectDate(String rectDate) {
		this.rectDate = rectDate;
	}
	public String getReprDate() {
		return reprDate;
	}
	public void setReprDate(String reprDate) {
		this.reprDate = reprDate;
	}
	public String getCmptDate() {
		return cmptDate;
	}
	public void setCmptDate(String cmptDate) {
		this.cmptDate = cmptDate;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public int getReprAcntSeqc() {
		return reprAcntSeqc;
	}
	public void setReprAcntSeqc(int reprAcntSeqc) {
		this.reprAcntSeqc = reprAcntSeqc;
	}
	public String getAcntNm() {
		return acntNm;
	}
	public void setAcntNm(String acntNm) {
		this.acntNm = acntNm;
	}
	public String getRqst() {
		return rqst;
	}
	public void setRqst(String rqst) {
		this.rqst = rqst;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNotiYn() {
		return notiYn;
	}
	public void setNotiYn(String notiYn) {
		this.notiYn = notiYn;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
}
