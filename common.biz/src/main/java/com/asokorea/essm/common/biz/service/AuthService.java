package com.asokorea.essm.common.biz.service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.asokorea.essm.common.biz.dao.essm.AdminMapper;
import com.asokorea.essm.common.biz.dao.meta.MetaMapper;
import com.asokorea.essm.common.biz.dto.CounterDTO;
import com.asokorea.essm.common.biz.dto.LoginDTO;
import com.asokorea.essm.common.biz.dto.ServiceDTO;
import com.asokorea.essm.common.biz.dto.UserDTO;
import com.asokorea.essm.common.biz.support.PropertyHelper;
import com.asokorea.essm.common.biz.support.SessionHelper;

@Service
public class AuthService {

	private final static Logger logger = LoggerFactory.getLogger(AuthService.class);
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private MetaMapper metaMapper;
	
	@Autowired
	private AdminMapper adminMapper;
	
	public LoginDTO login(
			@WebParam(name = "userId") String userId,
			@WebParam(name = "userPwd") String userPwd,
			@WebParam(name = "loginMode") String loginMode) throws Exception {
		
		// 로그인 결과 0:정상, 1:아이디 없음, 2:패스워드 틀림, 3:사용불가(미결제, 휴면, 불량), 4:사용자 정보 업데이트 오류
		HttpSession session = SessionHelper.getSession(request);
		LoginDTO loginDTO = new LoginDTO();
		ServiceDTO service = null;
		UserDTO user = null;
		int srvcSeqc = 0;

		service = metaMapper.getService(srvcSeqc, userId);
		
		if(service != null){
			srvcSeqc = service.getSrvcSeqc();
			user = metaMapper.getUser(0, userId);
			
			if(userPwd.equals(user.getLoginPassword())){
				
				user.setLastLoginDttm(new Date());
				int rtn = metaMapper.setUser(user);
				
				if(rtn == 1){
					loginDTO.setUserDTO(user);
					loginDTO.setServiceDTO(service);				
					loginDTO.setLoginResult(0);	// 정상 로그인
				}else{
					loginDTO.setLoginResult(4);	// 사용자 정보 업데이트 오류
				}
				
				SessionHelper.setLoginDTO(loginDTO, request);
			}else{
				loginDTO.setLoginResult(2);	// 패스워드 틀림
			}
		}else{
			loginDTO.setLoginResult(1);	// 아이디 없음
		}

		LoginDTO loginInfo = SessionHelper.getLoginDTO(request);
		String sessionUserId = (loginInfo != null) ? loginInfo.getUserId() : null;
		int sessionServiceSeq = (loginInfo != null) ? loginInfo.getSrvcSeqc() : 0;
		
		logger.info("############################ SET SESSION ################################");		
		logger.info("########## SESSION ID : " + session.getId());
		logger.info("########## 서비스 순번 : " + sessionServiceSeq);
		logger.info("########## 사용자 아이디 : " + sessionUserId);
		logger.info("############################ SET SESSION ################################");		
		
		return loginDTO;
	}
	
	public void logout() throws Exception {
		HttpSession session = SessionHelper.getSession(request);
		session.invalidate();
	}
	
	public int changePassword(
		@WebParam(name = "oldPassword") String oldPassword,
		@WebParam(name = "newPassword") String newPassword) throws Exception {
		
		int result = 0;	// 1 : 정상 처리 1이외 : 오류
		String userId = SessionHelper.getUserId(request);
		result = metaMapper.changePassword(userId, oldPassword, newPassword);
		return result;
	}

	public void setCounter(@WebParam(name = "counterNo") int counterNo) throws Exception {
		SessionHelper.getLoginDTO(request).setCounterNo(counterNo);
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("counterNo", counterNo);
		metaMapper.setCounter(map);
	}
	
	public CounterDTO getCouter(@WebParam(name = "counterNo") int counterNo)throws Exception {
		CounterDTO result = null;
		
		try {
//			int serviceID = SessionHelper.getServieSeqc(request);
//			String userId = SessionHelper.getUserId(request);
//			List<HttpSession> list = SessionListener.getSessionsByServiceID(serviceID);
			String useYn = "N";
			Map<String, Object> map = SessionHelper.getMapperParam(request);

			map.put("counterNo", counterNo);
			result = adminMapper.getCounter(map);
			
			if(result == null){
				result = new CounterDTO();
				result.setCloseYn("N");
				result.setRedyMony(0);
			}
				
//			if(list != null && list.size() > 0){
//				for (HttpSession httpSession : list) {
//					LoginDTO loginDTO = (LoginDTO) httpSession.getAttribute(SessionHelper.LOGIN_ATTRIBTE_NAME);
//					if(loginDTO.getCounterNo() == counterNo && !loginDTO.getUserId().equals(userId)){
//						useYn = "Y";
//						break;
//					}
//				}
//			}
			
			result.setCounterNo(counterNo);
			result.setCounterLabel(counterNo + "번 계산대");
			result.setUseYn(useYn);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public List<CounterDTO> getCounterList() throws Exception {
		List<CounterDTO> result = null;
		try {
			int maxCounterNo = SessionHelper.getLoginDTO(request).getServiceDTO().getMaxCounter();
			
			if(maxCounterNo == 1){	// 단일 계산대 사용
				CounterDTO dto = getCouter(1);
				dto.setUseYn("N");
				result = new ArrayList<CounterDTO>();
				result.add(dto);
			}else{
				
				for (int i = 1; i <= maxCounterNo; i++) {
					CounterDTO counterDTO = getCouter(i);
					
					if(result == null){
						result = new ArrayList<CounterDTO>();
					}
					
					result.add(counterDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public List<CounterDTO> getCounterDbList() throws Exception {
		List<CounterDTO> result = null;
		try {
			int maxCounterNo = SessionHelper.getLoginDTO(request).getServiceDTO().getMaxCounter();
			
			if(maxCounterNo == 1){	// 단일 계산대 사용
				CounterDTO dto = getCouter(1);
				dto.setUseYn("N");
				result = new ArrayList<CounterDTO>();
				result.add(dto);
			}else{
				
				for (int i = 1; i <= maxCounterNo; i++) {
					CounterDTO counterDTO = getCouter(i);
					
					if(result == null){
						result = new ArrayList<CounterDTO>();
					}
					
					result.add(counterDTO);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public Date getDate(@WebParam(name = "date") Date date) throws Exception {
		return new Date(date.getTime() + (3600 * 1000));
	}
	
	public String getVersion() throws Exception {
		return PropertyHelper.systemVersion;
	}
	
	public byte[] getLatestClient() throws Exception {
		byte[] result = null;
		Resource resource = new ClassPathResource("static/MRD.Setup.msi");
		File file = resource.getFile();
		Path path = file.toPath();
		result = Files.readAllBytes(path);
		return result;
	}
	
}
