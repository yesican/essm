package com.asokorea.essm.common.biz.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "SellDTO")
public class SellDTO {

	private int sellSeqc;
	private Date sellDt;
	private String sellDvsn;
	private int ctmrSeqc;
	private String ctmrNm;
	private String receiptNo;
	private int ttalSellMony;
	private int addDcntMony;
	private int pintUseMony;
	private int cstdCtfcUseMony;
	private int crdtMony;
	private int paymMony;
	private int cutrNum;
	private String slerId;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	private List<ProductSellDTO> productList;
	
	public int getSellSeqc() {
		return sellSeqc;
	}
	public void setSellSeqc(int sellSeqc) {
		this.sellSeqc = sellSeqc;
	}
	public Date getSellDt() {
		return sellDt;
	}
	public void setSellDt(Date sellDt) {
		this.sellDt = sellDt;
	}
	public String getSellDvsn() {
		return sellDvsn;
	}
	public void setSellDvsn(String sellDvsn) {
		this.sellDvsn = sellDvsn;
	}
	public int getCtmrSeqc() {
		return ctmrSeqc;
	}
	public void setCtmrSeqc(int ctmrSeqc) {
		this.ctmrSeqc = ctmrSeqc;
	}
	public String getCtmrNm() {
		return ctmrNm;
	}
	public void setCtmrNm(String ctmrNm) {
		this.ctmrNm = ctmrNm;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public int getTtalSellMony() {
		return ttalSellMony;
	}
	public void setTtalSellMony(int ttalSellMony) {
		this.ttalSellMony = ttalSellMony;
	}
	public int getAddDcntMony() {
		return addDcntMony;
	}
	public void setAddDcntMony(int addDcntMony) {
		this.addDcntMony = addDcntMony;
	}
	public int getPintUseMony() {
		return pintUseMony;
	}
	public void setPintUseMony(int pintUseMony) {
		this.pintUseMony = pintUseMony;
	}
	public int getCstdCtfcUseMony() {
		return cstdCtfcUseMony;
	}
	public void setCstdCtfcUseMony(int cstdCtfcUseMony) {
		this.cstdCtfcUseMony = cstdCtfcUseMony;
	}
	public int getCrdtMony() {
		return crdtMony;
	}
	public void setCrdtMony(int crdtMony) {
		this.crdtMony = crdtMony;
	}
	public int getPaymMony() {
		return paymMony;
	}
	public void setPaymMony(int paymMony) {
		this.paymMony = paymMony;
	}
	public int getCutrNum() {
		return cutrNum;
	}
	public void setCutrNum(int cutrNum) {
		this.cutrNum = cutrNum;
	}
	public String getSlerId() {
		return slerId;
	}
	public void setSlerId(String slerId) {
		this.slerId = slerId;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public List<ProductSellDTO> getProductList() {
		return productList;
	}
	public void setProductList(List<ProductSellDTO> productList) {
		this.productList = productList;
	}
//	public List<PaymentDTO> getPaymentList() {
//		return paymentList;
//	}
//	public void setPaymentList(List<PaymentDTO> paymentList) {
//		this.paymentList = paymentList;
//	}
}
