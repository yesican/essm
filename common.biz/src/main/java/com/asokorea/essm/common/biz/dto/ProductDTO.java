package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ProductDTO")
public class ProductDTO {
	
	private int prdtSeqc;
	private int mainAcntSeqc;
	private String cltsKind;
	private String prdtNm;
	private String frstBuyDay;
	private int frstBuyPrce;
	private int sellPrce;
	private int tagPrtgPrce;
	private String year;
	private String sesn;
	private String pintAccmYn;
	private String sellStts;
	private String imgFileNm;
	private String refcCntt;
	
	private int prdtMstrSeqc;
	private String clor;
	private String size;
	private String barcode;
	
	private int unitPrce;
	private int ttalStckQntt;
	private int evntSeqc;
	private int sellQntt;
	private int orderQntt;
	private int buyQntt;
	private int qntt;
	private int dcntRate;
	
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public int getMainAcntSeqc() {
		return mainAcntSeqc;
	}
	public void setMainAcntSeqc(int mainAcntSeqc) {
		this.mainAcntSeqc = mainAcntSeqc;
	}
	public String getCltsKind() {
		return cltsKind;
	}
	public void setCltsKind(String cltsKind) {
		this.cltsKind = cltsKind;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getFrstBuyDay() {
		return frstBuyDay;
	}
	public void setFrstBuyDay(String frstBuyDay) {
		this.frstBuyDay = frstBuyDay;
	}
	public int getFrstBuyPrce() {
		return frstBuyPrce;
	}
	public void setFrstBuyPrce(int frstBuyPrce) {
		this.frstBuyPrce = frstBuyPrce;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public int getTagPrtgPrce() {
		return tagPrtgPrce;
	}
	public void setTagPrtgPrce(int tagPrtgPrce) {
		this.tagPrtgPrce = tagPrtgPrce;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getSesn() {
		return sesn;
	}
	public void setSesn(String sesn) {
		this.sesn = sesn;
	}
	public String getPintAccmYn() {
		return pintAccmYn;
	}
	public void setPintAccmYn(String pintAccmYn) {
		this.pintAccmYn = pintAccmYn;
	}
	public String getSellStts() {
		return sellStts;
	}
	public void setSellStts(String sellStts) {
		this.sellStts = sellStts;
	}
	public String getImgFileNm() {
		return imgFileNm;
	}
	public void setImgFileNm(String imgFileNm) {
		this.imgFileNm = imgFileNm;
	}
	public String getRefcCntt() {
		return refcCntt;
	}
	public void setRefcCntt(String refcCntt) {
		this.refcCntt = refcCntt;
	}
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public int getUnitPrce() {
		return unitPrce;
	}
	public void setUnitPrce(int unitPrce) {
		this.unitPrce = unitPrce;
	}
	public int getTtalStckQntt() {
		return ttalStckQntt;
	}
	public void setTtalStckQntt(int ttalStckQntt) {
		this.ttalStckQntt = ttalStckQntt;
	}
	public int getEvntSeqc() {
		return evntSeqc;
	}
	public void setEvntSeqc(int evntSeqc) {
		this.evntSeqc = evntSeqc;
	}
	public int getSellQntt() {
		return sellQntt;
	}
	public void setSellQntt(int sellQntt) {
		this.sellQntt = sellQntt;
	}
	public int getOrderQntt() {
		return orderQntt;
	}
	public void setOrderQntt(int orderQntt) {
		this.orderQntt = orderQntt;
	}
	public int getBuyQntt() {
		return buyQntt;
	}
	public void setBuyQntt(int buyQntt) {
		this.buyQntt = buyQntt;
	}
	public int getQntt() {
		return qntt;
	}
	public void setQntt(int qntt) {
		this.qntt = qntt;
	}
	public int getDcntRate() {
		return dcntRate;
	}
	public void setDcntRate(int dcntRate) {
		this.dcntRate = dcntRate;
	}

}
