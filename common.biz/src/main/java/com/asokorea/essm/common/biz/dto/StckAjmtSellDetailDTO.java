package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "StckAjmtSellDetailDTO")
public class StckAjmtSellDetailDTO {

	private int sellSeqc;	//판매순번
	private int listSeqc;	//정렬순번
	private int prdtSeqc;	//상품코드
	private int unitPrce;	//단가
	private int dcntRate;	//할인율
	private int evntSeqc;	//행사순번
	private int sellPrce;	//판매가
	private int sellQntt;	//판매수량
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	private String sellDvsn;//판매구분
	
	public int getSellSeqc() {
		return sellSeqc;
	}
	public void setSellSeqc(int sellSeqc) {
		this.sellSeqc = sellSeqc;
	}
	public int getListSeqc() {
		return listSeqc;
	}
	public void setListSeqc(int listSeqc) {
		this.listSeqc = listSeqc;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public int getUnitPrce() {
		return unitPrce;
	}
	public void setUnitPrce(int unitPrce) {
		this.unitPrce = unitPrce;
	}
	public int getDcntRate() {
		return dcntRate;
	}
	public void setDcntRate(int dcntRate) {
		this.dcntRate = dcntRate;
	}
	public int getEvntSeqc() {
		return evntSeqc;
	}
	public void setEvntSeqc(int evntSeqc) {
		this.evntSeqc = evntSeqc;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public int getSellQntt() {
		return sellQntt;
	}
	public void setSellQntt(int sellQntt) {
		this.sellQntt = sellQntt;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	public String getSellDvsn() {
		return sellDvsn;
	}
	public void setSellDvsn(String sellDvsn) {
		this.sellDvsn = sellDvsn;
	}
}
