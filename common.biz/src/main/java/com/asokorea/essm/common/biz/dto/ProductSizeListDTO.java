package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ProductSizeListDTO")
public class ProductSizeListDTO {

	private int prdtMstrSeqc;
	private String size;
	
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
}
