package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ServiceDTO")
public class ServiceDTO {

	private int srvcSeqc;
	private String srvcNm;
	private String expireDate;
	private int maxConnection;
	private String shopName;
	private String phone;
	private String email;
	private String taxId;
	private String addr;
	private String nonPaymentYn;
	private int dupEventPrice;
	private String pointYn;
	private String vanId;
	private int maxCounter;
	private int maxDepositDay;
	private String repairMessage;
	private String note;
	private int pointRate;
	private String useYn;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getSrvcNm() {
		return srvcNm;
	}
	public void setSrvcNm(String srvcNm) {
		this.srvcNm = srvcNm;
	}
	public String getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	public int getMaxConnection() {
		return maxConnection;
	}
	public void setMaxConnection(int maxConnection) {
		this.maxConnection = maxConnection;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getNonPaymentYn() {
		return nonPaymentYn;
	}
	public void setNonPaymentYn(String nonPaymentYn) {
		this.nonPaymentYn = nonPaymentYn;
	}
	public int getDupEventPrice() {
		return dupEventPrice;
	}
	public void setDupEventPrice(int dupEventPrice) {
		this.dupEventPrice = dupEventPrice;
	}
	public String getPointYn() {
		return pointYn;
	}
	public void setPointYn(String pointYn) {
		this.pointYn = pointYn;
	}
	public String getVanId() {
		return vanId;
	}
	public void setVanId(String vanId) {
		this.vanId = vanId;
	}
	public int getMaxCounter() {
		return maxCounter;
	}
	public void setMaxCounter(int maxCounter) {
		this.maxCounter = maxCounter;
	}
	public int getMaxDepositDay() {
		return maxDepositDay;
	}
	public void setMaxDepositDay(int maxDepositDay) {
		this.maxDepositDay = maxDepositDay;
	}
	public String getRepairMessage() {
		return repairMessage;
	}
	public void setRepairMessage(String repairMessage) {
		this.repairMessage = repairMessage;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getPointRate() {
		return pointRate;
	}
	public void setPointRate(int pointRate) {
		this.pointRate = pointRate;
	}	
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
}
