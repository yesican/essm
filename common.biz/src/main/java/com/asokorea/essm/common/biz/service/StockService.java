package com.asokorea.essm.common.biz.service;

import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asokorea.essm.common.biz.dao.essm.StockMapper;
import com.asokorea.essm.common.biz.dto.BuyDetailRegsDTO;
import com.asokorea.essm.common.biz.dto.BuyRegsDTO;
import com.asokorea.essm.common.biz.dto.OrderBuyMainDTO;
import com.asokorea.essm.common.biz.dto.OrdrBuyDetailDTO;
import com.asokorea.essm.common.biz.dto.OrdrDcmtRegsDTO;
import com.asokorea.essm.common.biz.dto.OrdrRegsDTO;
import com.asokorea.essm.common.biz.dto.OrdrRegsSearchDTO;
import com.asokorea.essm.common.biz.dto.ProductColListDTO;
import com.asokorea.essm.common.biz.dto.ProductSizeListDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtDetailRegsDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtProdSearchDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtRegsDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtSellDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtSellDetailDTO;
import com.asokorea.essm.common.biz.dto.StockListDTO;
import com.asokorea.essm.common.biz.dto.StockListSearchDTO;
import com.asokorea.essm.common.biz.support.PropertyHelper;
import com.asokorea.essm.common.biz.support.SessionHelper;

@Service
public class StockService {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private StockMapper stockMapper;

	public List<OrdrRegsDTO> getOrdrRegsList(@WebParam(name = "ordrRegsSearchDTO") OrdrRegsSearchDTO ordrRegsSearchDTO)
			throws Exception {
		List<OrdrRegsDTO> result = null;
		result = stockMapper.getOrdrRegsList(ordrRegsSearchDTO, PropertyHelper.summayPrevDays);
		return result;
	}

	public int addOrderDocument(@WebParam(name = "ordrDcmtRegsDTO") OrdrDcmtRegsDTO ordrDcmtRegsDTO,
			@WebParam(name = "ordrRegsDTO") OrdrRegsDTO[] ordrRegsList) throws Exception {

		int result = 0;

		int srvcSeqc = SessionHelper.getServieSeqc(request);

		ordrDcmtRegsDTO.setSrvcSeqc(srvcSeqc);
		stockMapper.addOrderDocument(ordrDcmtRegsDTO);

		int ordrDcmtSeqc = ordrDcmtRegsDTO.getOrdrDcmt();
		if (ordrDcmtSeqc > 0) {
			if (ordrRegsList != null && ordrRegsList.length > 0) {
				for (OrdrRegsDTO ordrRegsDTO : ordrRegsList) {
					ordrRegsDTO.setOrdrDcmt(ordrDcmtSeqc);
					ordrRegsDTO.setRegsUserId(ordrDcmtRegsDTO.getRegsUserId());
					ordrRegsDTO.setUpdtUserId(ordrDcmtRegsDTO.getUpdtUserId());

					stockMapper.addOrderDocumentDetail(ordrRegsDTO);
				}
			}
			result = ordrDcmtSeqc;
		} else {
			result = 0;
			throw new Exception("신규 이벤트 생성 오류");
		}

		return result;
	}

	public List<OrdrRegsDTO> getBasketList() throws Exception {
		List<OrdrRegsDTO> result = null;
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		result = stockMapper.getBasketList(srvcSeqc, PropertyHelper.summayPrevDays);
		return result;
	}

	public int addBuyBasket(@WebParam(name = "prdtSeqc") int prdtSeqc, @WebParam(name = "ordrQntt") int ordrQntt)
			throws Exception {
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("prdtSeqc", prdtSeqc);
		map.put("ordrQntt", ordrQntt);
		int result = stockMapper.addBuyBasket(map);
		return result;
	}

	public int delBasket(@WebParam(name = "prdtSeqc") int prdtSeqc) throws Exception {
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		int result = stockMapper.delBasket(prdtSeqc, srvcSeqc);
		return result;
	}

	public int delBasketList(@WebParam(name = "ordrRegsDTO") OrdrRegsDTO[] ordrRegsDTO) throws Exception {
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		int result = 0;

		if (ordrRegsDTO != null && ordrRegsDTO.length > 0) {

			for (OrdrRegsDTO item : ordrRegsDTO) {
				int prdtSeqc = item.getPrdtSeqc();
				stockMapper.delBasket(prdtSeqc, srvcSeqc);
			}

			result = 1;
		}

		return result;
	}

	public int setBasketUseYn(OrdrRegsDTO[] ordrRegsDTOlList) throws Exception {

		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);

		for (OrdrRegsDTO ordrRegsDTO : ordrRegsDTOlList) {
			map.put("ordrRegsDTO", ordrRegsDTO);
			result += stockMapper.setBasketUseYn(map);
		}

		return result;
	}

	public List<BuyRegsDTO> getBuyList(@WebParam(name = "buyDay") String buyDay) throws Exception {
		List<BuyRegsDTO> result = null;
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		result = stockMapper.getBuyList(buyDay, srvcSeqc);
		return result;
	}

	public List<BuyDetailRegsDTO> getBuyDetailList(@WebParam(name = "buySeqc") int buySeqc) throws Exception {
		List<BuyDetailRegsDTO> result = null;
		result = stockMapper.getBuyDetailList(buySeqc);
		return result;
	}

	public List<ProductColListDTO> getBuyRegsClorList(@WebParam(name = "prdtMstrSeqc") int prdtMstrSeqc)
			throws Exception {
		List<ProductColListDTO> result = null;
		result = stockMapper.getBuyRegsClorList(prdtMstrSeqc);
		return result;
	}

	public List<ProductSizeListDTO> getBuyRegsSzieList(@WebParam(name = "prdtMstrSeqc") int prdtMstrSeqc)
			throws Exception {
		List<ProductSizeListDTO> result = null;
		result = stockMapper.getBuyRegsSzieList(prdtMstrSeqc);
		return result;
	}

	public List<BuyDetailRegsDTO> getBuyRegsProductList(@WebParam(name = "prdtNm") String prdtNm) throws Exception {
		List<BuyDetailRegsDTO> result = null;
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		result = stockMapper.getBuyRegsProductList(srvcSeqc, prdtNm);
		return result;
	}

	public int addBuy(@WebParam(name = "buyRegsDTO") BuyRegsDTO buyRegsDTO,
			@WebParam(name = "buyDetailRegsList") BuyDetailRegsDTO[] buyDetailRegsList,
			@WebParam(name = "paymMony") int paymMony) throws Exception {

		int result = 0;
		int buySeqc = 0;

		int srvcSeqc = SessionHelper.getServieSeqc(request);
		String userId = SessionHelper.getUserId(request);

		buyRegsDTO.setSrvcSeqc(srvcSeqc);
		buyRegsDTO.setUseYn("Y");
		buyRegsDTO.setRegsUserId(userId);
		buyRegsDTO.setUpdtUserId(userId);

		stockMapper.addBuy(buyRegsDTO);
		buySeqc = buyRegsDTO.getBuySeqc();

		int buyMony = 0;
		int acntSeqc = buyRegsDTO.getAcntSeqc();

		if (buySeqc > 0) {

			for (BuyDetailRegsDTO buyDetailRegsDTO : buyDetailRegsList) {
				int buyDtlSeqc = buyDetailRegsDTO.getBuyDtlSeqc();
				int qntt = buyDetailRegsDTO.getBuyQntt();
				int qnttOld = buyDetailRegsDTO.getBuyQnttOld();
				int price = buyDetailRegsDTO.getBuyPrice();
				int priceOld = buyDetailRegsDTO.getBuyPriceOld();

				buyMony += (price * qntt);

				if (buyDtlSeqc == 0 || qntt != qnttOld || price != priceOld) {

					buyDetailRegsDTO.setBuySeqc(buySeqc);
					buyDetailRegsDTO.setRegsUserId(userId);
					buyDetailRegsDTO.setRegsUserId(userId);

					result = stockMapper.addOrSetBuyDetail(buyDetailRegsDTO);

					if (result <= 0) {
						throw new Exception("사입 상세 등록 오류");
					}
				}
			}

			Map<String, Object> map = SessionHelper.getMapperParam(request);
			map.put("buySeqc", buySeqc);
			map.put("acntSeqc", acntSeqc);
			map.put("buyMony", buyMony);
			map.put("paymMony", paymMony);
			int payResult = stockMapper.addBuyPay(map);

			if (payResult > 0) {
				result = 1;
			} else {
				throw new Exception("사입대금 등록 오류");
			}

		} else {
			throw new Exception("사입 마스터 등록 오류");
		}

		result = buySeqc;
		return result;
	}

	public int setBuy(@WebParam(name = "buyRegsDTO") BuyRegsDTO buyRegsDTO,
			@WebParam(name = "buyDetailRegsList") BuyDetailRegsDTO[] buyDetailRegsList,
			@WebParam(name = "paymMony") int paymMony) throws Exception {

		int result = 0;
		int buySeqc = 0;

		int srvcSeqc = SessionHelper.getServieSeqc(request);
		String userId = SessionHelper.getUserId(request);

		buyRegsDTO.setSrvcSeqc(srvcSeqc);
		buyRegsDTO.setUseYn("Y");
		buyRegsDTO.setRegsUserId(userId);
		buyRegsDTO.setUpdtUserId(userId);

		buySeqc = buyRegsDTO.getBuySeqc();
		result = stockMapper.setBuy(buyRegsDTO);

		int buyMony = 0;
//		int acntSeqc = buyRegsDTO.getAcntSeqc();

		if (result > 0 && buySeqc > 0) {

			int detailResult = 0;
			result = 0;

			for (BuyDetailRegsDTO buyDetailRegsDTO : buyDetailRegsList) {
				int buyDtlSeqc = buyDetailRegsDTO.getBuyDtlSeqc();
				int qntt = buyDetailRegsDTO.getBuyQntt();
				int qnttOld = buyDetailRegsDTO.getBuyQnttOld();
				int price = buyDetailRegsDTO.getBuyPrice();
				int priceOld = buyDetailRegsDTO.getBuyPriceOld();

				buyMony += (price * qntt);

				if (buyDtlSeqc == 0 || qntt != qnttOld || price != priceOld) {

					buyDetailRegsDTO.setBuySeqc(buySeqc);
					buyDetailRegsDTO.setRegsUserId(userId);
					buyDetailRegsDTO.setRegsUserId(userId);

					detailResult = stockMapper.addOrSetBuyDetail(buyDetailRegsDTO);

					if (detailResult <= 0) {
						throw new Exception("사입 상세 수정 오류");
					}
				}
			}

			Map<String, Object> map = SessionHelper.getMapperParam(request);
			map.put("buySeqc", buySeqc);
			map.put("buyMony", buyMony);
			map.put("paymMony", paymMony);
			int payResult = stockMapper.setBuyPay(map);

			if (payResult > 0) {
				result = 1;
			} else {
				throw new Exception("사입대금 등록 오류");
			}

		} else {
			result = 0;
			throw new Exception("사입 마스터 수정 오류");
		}

		return result;
	}

	public int addBuyDetail(@WebParam(name = "buyDetailRegsDTO") BuyDetailRegsDTO[] buyDetailRegsDTO) throws Exception {

		int result = 0;

		for (BuyDetailRegsDTO detailRegsDTO : buyDetailRegsDTO) {
			result += stockMapper.addBuyDetail(detailRegsDTO);
		}

		return result;
	}

	public int setBuyDetail(@WebParam(name = "buyDetailRegsDTO") BuyDetailRegsDTO[] buyDetailRegsDTO) throws Exception {

		int result = 0;

		for (BuyDetailRegsDTO detailRegsDTO : buyDetailRegsDTO) {
			result += stockMapper.setBuyDetail(detailRegsDTO);
		}

		return result;
	}

	public int delBuyDetail(@WebParam(name = "buyDtlSeqc") int buyDtlSeqc) throws Exception {
		int result = 0;
		result = stockMapper.delBuyDetail(buyDtlSeqc);
		return result;
	}

	public List<BuyDetailRegsDTO> getBuyProductSetList(@WebParam(name = "buySeqc") int buySeqc,
			@WebParam(name = "prdtMstrSeqc") int prdtMstrSeqc) throws Exception {
		List<BuyDetailRegsDTO> result = null;
		result = stockMapper.getBuyProductSetList(buySeqc, prdtMstrSeqc);
		return result;
	}

	public int getBuyProductPrdtSeqc(@WebParam(name = "prdtNm") String prdtNm, @WebParam(name = "clor") String clor,
			@WebParam(name = "size") String size) throws Exception {
		int result = 0;
		result = stockMapper.getBuyProductPrdtSeqc(prdtNm, clor, size);
		return result;
	}

	public List<OrderBuyMainDTO> getOrderBuyMainList(@WebParam(name = "mainAcntSeqc") int mainAcntSeqc,
			@WebParam(name = "strtOrdrDay") String strtOrdrDay, @WebParam(name = "endOrdrDay") String endOrdrDay)
			throws Exception {
		List<OrderBuyMainDTO> result = null;
		result = stockMapper.getOrderBuyMainList(mainAcntSeqc, strtOrdrDay, endOrdrDay);
		return result;
	}

	public List<OrdrBuyDetailDTO> getOrderBuyDetailList(@WebParam(name = "mainAcntSeqc") int mainAcntSeqc,
			@WebParam(name = "ordrDay") String ordrDay) throws Exception {
		List<OrdrBuyDetailDTO> result = null;
		result = stockMapper.getOrderBuyDetailList(mainAcntSeqc, ordrDay);
		return result;
	}

	public int addOrderBuy(@WebParam(name = "buyRegsDTO") BuyRegsDTO buyRegsDTO,
			@WebParam(name = "buyDetailRegsDTO") BuyDetailRegsDTO[] buyDetailRegsDTO,
			@WebParam(name = "ordrBuyDetailDTO") OrdrBuyDetailDTO[] ordrBuyDetailDTO,
			@WebParam(name = "ordrDay") String ordrDay, @WebParam(name = "mainAcntSeqc") int mainAcntSeqc)
			throws Exception {

		int result = 0;

		int srvcSeqc = SessionHelper.getServieSeqc(request);

		buyRegsDTO.setSrvcSeqc(srvcSeqc);
		stockMapper.addBuy(buyRegsDTO);

		int buySeqc = buyRegsDTO.getBuySeqc();

		if (buySeqc > 0) {
			if (buyDetailRegsDTO != null && buyDetailRegsDTO.length > 0) {
				for (BuyDetailRegsDTO buyDetailList : buyDetailRegsDTO) {
					buyDetailList.setBuySeqc(buySeqc);
					stockMapper.addBuyDetail(buyDetailList);
				}
			}

			if (ordrBuyDetailDTO != null && ordrBuyDetailDTO.length > 0) {
				for (OrdrBuyDetailDTO ordrDetailList : ordrBuyDetailDTO) {
					stockMapper.setOrderDocumentDetails(ordrDetailList, ordrDay, mainAcntSeqc);
				}
			}
			result = buySeqc;
		} else {
			result = 0;
			throw new Exception("신규 주문입고 생성 실패");
		}

		return result;
	}

	public List<StockListDTO> getStockList(@WebParam(name = "stockListSearchDTO") StockListSearchDTO stockListSearchDTO)
			throws Exception {
		List<StockListDTO> result = null;
		int srvceSeqc = SessionHelper.getServieSeqc(request);
		stockListSearchDTO.setSrvcSeqc(srvceSeqc);
		result = stockMapper.getStockList(stockListSearchDTO);
		return result;
	}

	public List<StckAjmtRegsDTO> getStckAjmtList(@WebParam(name = "year") String year) throws Exception {
		List<StckAjmtRegsDTO> result = null;
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		result = stockMapper.getStckAjmtList(year, srvcSeqc);
		return result;
	}

	public List<StckAjmtDetailRegsDTO> getStckAjmtDetailList(@WebParam(name = "stckAjmtSeqc") int stckAjmtSeqc)
			throws Exception {
		List<StckAjmtDetailRegsDTO> result = null;
		result = stockMapper.getStckAjmtDetailList(stckAjmtSeqc, PropertyHelper.summayPrevDays);
		return result;
	}

	public List<StckAjmtDetailRegsDTO> getStckAjmtProdList(
			@WebParam(name = "stckAjmtProdSearchDTO") StckAjmtProdSearchDTO stckAjmtProdSearchDTO) throws Exception {
		List<StckAjmtDetailRegsDTO> result = null;
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		stckAjmtProdSearchDTO.setSrvcSeqc(srvcSeqc);
		result = stockMapper.getStckAjmtProdList(stckAjmtProdSearchDTO, PropertyHelper.summayPrevDays);
		return result;
	}

	public int addStckAjmt(@WebParam(name = "stckAjmtRegsDTO") StckAjmtRegsDTO stckAjmtRegsDTO,
			@WebParam(name = "stckAjmtDetailRegsDTO") StckAjmtDetailRegsDTO[] stckAjmtDetailRegsList) throws Exception {

		int result = 0;
		int srvcSeqc = SessionHelper.getServieSeqc(request);

		stckAjmtRegsDTO.setSrvcSeqc(srvcSeqc);
		stockMapper.addStckAjmt(stckAjmtRegsDTO);

		int stckAjmtSeqc = stckAjmtRegsDTO.getStckAjmtSeqc();
		if (stckAjmtSeqc > 0) {
			if (stckAjmtDetailRegsList != null && stckAjmtDetailRegsList.length > 0) {
				for (StckAjmtDetailRegsDTO stckAjmtDetailRegsDTO : stckAjmtDetailRegsList) {
					stckAjmtDetailRegsDTO.setStckAjmtSeqc(stckAjmtSeqc);
					stckAjmtDetailRegsDTO.setRegsUserId(stckAjmtRegsDTO.getRegsUserId());
					stckAjmtDetailRegsDTO.setUpdtUserId(stckAjmtRegsDTO.getUpdtUserId());

					stockMapper.addStckAjmtDetail(stckAjmtDetailRegsDTO);
				}
			}
			result = stckAjmtSeqc;
		} else {
			result = 0;
			throw new Exception("생성 오류");
		}

		return result;
	}

	public int setStckAjmt(@WebParam(name = "stckAjmtRegsDTO") StckAjmtRegsDTO stckAjmtRegsDTO,
			@WebParam(name = "stckAjmtDetailRegsDTO") StckAjmtDetailRegsDTO[] stckAjmtDetailRegsList) throws Exception {

		int result = 0;
		int srvcSeqc = SessionHelper.getServieSeqc(request);

		stckAjmtRegsDTO.setSrvcSeqc(srvcSeqc);
		stockMapper.setStckAjmt(stckAjmtRegsDTO);

		int stckAjmtSeqc = stckAjmtRegsDTO.getStckAjmtSeqc();
		if (stckAjmtSeqc > 0) {
			if (stckAjmtDetailRegsList != null && stckAjmtDetailRegsList.length > 0) {
				for (StckAjmtDetailRegsDTO stckAjmtDetailRegsDTO : stckAjmtDetailRegsList) {

					stckAjmtDetailRegsDTO.setStckAjmtSeqc(stckAjmtSeqc);
					stckAjmtDetailRegsDTO.setUpdtUserId(stckAjmtRegsDTO.getUpdtUserId());

					if (stckAjmtDetailRegsDTO.getStckAjmtDtlSeqc() != 0) {
						stockMapper.setStckAjmtDetail(stckAjmtDetailRegsDTO);
					} else {
						stckAjmtDetailRegsDTO.setRegsUserId(stckAjmtRegsDTO.getRegsUserId());
						stockMapper.addStckAjmtDetail(stckAjmtDetailRegsDTO);
					}

				}
			}
			result = stckAjmtSeqc;
		} else {
			result = 0;
			throw new Exception("생성 오류");
		}

		return result;
	}

	public int addStckAjmtSell(@WebParam(name = "stckAjmtSellDTO") StckAjmtSellDTO[] stckAjmtSellDTOList,
			@WebParam(name = "stckAjmtSellDetailDTO") StckAjmtSellDetailDTO[] stckAjmtSellDetailDTOList)
			throws Exception {

		int result = 0;
		int sellSeqc = 0;
		String dvsn = "";
		String dvsnDetail = "";
		int srvcSeqc = SessionHelper.getServieSeqc(request);

		for (StckAjmtSellDTO stckAjmtSellDTO : stckAjmtSellDTOList) {
			stckAjmtSellDTO.setSrvcSeqc(srvcSeqc);
			stockMapper.addStckAjmtSell(stckAjmtSellDTO);

			sellSeqc = stckAjmtSellDTO.getSellSeqc();
			dvsn = stckAjmtSellDTO.getSellDvsn();

			if (sellSeqc > 0) {
				for (StckAjmtSellDetailDTO stckAjmtSellDetailDTO : stckAjmtSellDetailDTOList) {
					dvsnDetail = stckAjmtSellDetailDTO.getSellDvsn();
					stckAjmtSellDetailDTO.setSellSeqc(sellSeqc);

					if (dvsnDetail.equals(dvsn)) {
						stockMapper.addStckAjmtSellDetail(stckAjmtSellDetailDTO);
					}
				}
			}
		}

		result = sellSeqc;
		return result;
	}

	public List<StckAjmtDetailRegsDTO> getStckAjmtProdBarcodeList(@WebParam(name = "barcode") String barcode)
			throws Exception {
		List<StckAjmtDetailRegsDTO> result = null;
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		result = stockMapper.getStckAjmtProdBarcodeList(srvcSeqc, barcode, PropertyHelper.summayPrevDays);
		return result;
	}

	public int addBuyPay(@WebParam(name = "buySeqc") int buySeqc, @WebParam(name = "acntSeqc") int acntSeqc,
			@WebParam(name = "buyMony") int buyMony, @WebParam(name = "paymMony") int paymMony) throws Exception {

		int result = 0;

		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("buySeqc", buySeqc);
		map.put("acntSeqc", acntSeqc);
		map.put("buyMony", buyMony);
		map.put("paymMony", paymMony);
		result = stockMapper.addBuyPay(map);
		return result;
	}

	public int setBuyPay(@WebParam(name = "nonPaymMonySeqc") int nonPaymMonySeqc,
			@WebParam(name = "buySeqc") int buySeqc, @WebParam(name = "buyMony") int buyMony,
			@WebParam(name = "paymMony") int paymMony) throws Exception {

		int result = 0;

		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("nonPaymMonySeqc", nonPaymMonySeqc);
		map.put("buySeqc", buySeqc);
		map.put("buyMony", buyMony);
		map.put("paymMony", paymMony);
		result = stockMapper.setBuyPay(map);
		return result;
	}
}
