package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="CounterDTO")
public class CounterDTO {
	
	private int counterNo;
	private String counterLabel;
	private String useYn;
	private String closeYn;
	private int redyMony;
	
	public int getCounterNo() {
		return counterNo;
	}
	public void setCounterNo(int counterNo) {
		this.counterNo = counterNo;
	}
	public String getCounterLabel() {
		return counterLabel;
	}
	public void setCounterLabel(String counterLabel) {
		this.counterLabel = counterLabel;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getCloseYn() {
		return closeYn;
	}
	public void setCloseYn(String closeYn) {
		this.closeYn = closeYn;
	}
	public int getRedyMony() {
		return redyMony;
	}
	public void setRedyMony(int redyMony) {
		this.redyMony = redyMony;
	}
	
}
