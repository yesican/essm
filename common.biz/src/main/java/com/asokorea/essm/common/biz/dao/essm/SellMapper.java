package com.asokorea.essm.common.biz.dao.essm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.asokorea.essm.common.biz.dto.CustodyCertificateDTO;
import com.asokorea.essm.common.biz.dto.CustodyCertificateTtalDTO;
import com.asokorea.essm.common.biz.dto.CustomerDTO;
import com.asokorea.essm.common.biz.dto.PaymentDTO;
import com.asokorea.essm.common.biz.dto.ProductSellDTO;
import com.asokorea.essm.common.biz.dto.ProductSellHistoryDTO;
import com.asokorea.essm.common.biz.dto.SellAnalysisDTO;
import com.asokorea.essm.common.biz.dto.SellAnalysisSearchDTO;
import com.asokorea.essm.common.biz.dto.SellDTO;

@Mapper
public interface SellMapper {

	public List<SellAnalysisDTO> getSellAnalysisList(
			@Param("sellAnalysisSearchDTO") SellAnalysisSearchDTO sellAnalysisSearchDTO,
			@Param("summayPrevDays") int summayPrevDays) throws Exception;

	public List<Integer> getProductSearchCount(
			@Param("srvcSeqc") int srvcSeqc,
			@Param("barcode") String barcode,
			@Param("prdtNm") String prdtNm) throws Exception;
	
	public List<ProductSellDTO> getProductList(
			@Param("srvcSeqc") int srvcSeqc,
			@Param("prdtSeqc") int prdtSeqc,
			@Param("barcode") String barcode,
			@Param("prdtNm") String prdtNm,
			@Param("dupEventPrice") int dupEventPrice,
			@Param("summayPrevDays") int summayPrevDays) throws Exception;

	public List<ProductSellDTO> getProductExchangeList(
			@Param("srvcSeqc") int srvcSeqc,
			@Param("prdtSeqc") int prdtSeqc,
			@Param("barcode") String barcode,
			@Param("prdtNm") String prdtNm,
			@Param("dupEventPrice") int dupEventPrice,
			@Param("summayPrevDays") int summayPrevDays) throws Exception;
	
	public List<CustomerDTO> getSellCustomerList(
			@Param("srvcSeqc") int srvcSeqc,
			@Param("ctmrSeqc") int ctmrSeqc,
			@Param("ctmrNm") String ctmrNm,
			@Param("mbilPhneNum") String mbilPhneNum) throws Exception;
	
	public CustomerDTO getCustomer(@Param("ctmrSeqc") int ctmrSeqc) throws Exception;

	public List<ProductSellDTO> getProductStock(
			@Param("prdtMstrSeqc") int prdtMstrSeqc,
			@Param("srvcSeqc") int srvcSeqc,
			@Param("summayPrevDays") int summayPrevDays) throws Exception;
	
	public List<CustodyCertificateTtalDTO> getCustodyCertificateTtal(
			@Param("year") String year,
			@Param("srvcSeqc") int srvcSeqc) throws Exception;
	
	public List<CustodyCertificateDTO> getCustodyCertificate(
			@Param("yymm") String yymm,
			@Param("srvcSeqc") int srvcSeqc) throws Exception;

	public int addSell(Map<String, Object> map) throws Exception;

	public int addSellDetail(Map<String, Object> map) throws Exception;
	
	public CustodyCertificateDTO getCustodyCertificateInfo(
			@Param("cstdCtfcNo") String cstdCtfcNo,
			@Param("srvcSeqc") int srvcSeqc) throws Exception;
	
	public int setCustodyCertificate(
			@Param("cstdCtfcSeqc") int cstdCtfcSeqc,
			@Param("srvcSeqc") int srvcSeqc) throws Exception;
	
	public int delCustodyCertificate(
			@Param("cstdCtfcSeqc") int cstdCtfcSeqc, 
			@Param("srvcSeqc") int srvcSeqc) throws Exception;

	public int addPay(Map<String, Object> map) throws Exception;

	public HashMap<String, Object> getReceiptNo(@Param("sellSeqc") int sellSeqc) throws Exception;

	public int addPoint(Map<String, Object> map) throws Exception;

	public List<SellDTO> getSellList(Map<String, Object> map) throws Exception;
	
	public List<ProductSellDTO> getReceiptProductList(Map<String, Object> map) throws Exception;

	public List<PaymentDTO> getPaymentList(Map<String, Object> map) throws Exception;

	public int addCstdCtfc(Map<String, Object> map) throws Exception;

	public int setCstdCtfc(Map<String, Object> map) throws Exception;

	public List<ProductSellDTO> getSellProductList(Map<String, Object> map) throws Exception;

	public List<ProductSellHistoryDTO> getSellHistory(Map<String, Object> map) throws Exception;

}
