package com.asokorea.essm.common.biz.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asokorea.essm.common.biz.dao.essm.CustomerMapper;
import com.asokorea.essm.common.biz.dao.essm.SellMapper;
import com.asokorea.essm.common.biz.dto.CustodyCertificateDTO;
import com.asokorea.essm.common.biz.dto.CustodyCertificateTtalDTO;
import com.asokorea.essm.common.biz.dto.CustomerDTO;
import com.asokorea.essm.common.biz.dto.LoginDTO;
import com.asokorea.essm.common.biz.dto.PaymentDTO;
import com.asokorea.essm.common.biz.dto.ProductSellDTO;
import com.asokorea.essm.common.biz.dto.ProductSellHistoryDTO;
import com.asokorea.essm.common.biz.dto.SellAnalysisDTO;
import com.asokorea.essm.common.biz.dto.SellAnalysisSearchDTO;
import com.asokorea.essm.common.biz.dto.SellDTO;
import com.asokorea.essm.common.biz.support.PropertyHelper;
import com.asokorea.essm.common.biz.support.SessionHelper;

@Service
public class SellService {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private SellMapper sellMapper;

	@Autowired
	private CustomerMapper customerMapper;

	public List<SellAnalysisDTO> getSellAnalysisList(
			@WebParam(name = "sellAnalysisSearchDTO") SellAnalysisSearchDTO sellAnalysisSearchDTO) throws Exception {
		List<SellAnalysisDTO> result = null;
		result = sellMapper.getSellAnalysisList(sellAnalysisSearchDTO, PropertyHelper.summayPrevDays);
		return result;
	}

	public List<ProductSellDTO> getProductList(@WebParam(name = "srvcSeqc") int srvcSeqc,
			@WebParam(name = "prdtSeqc") int prdtSeqc, @WebParam(name = "barcode") String barcode,
			@WebParam(name = "prdtNm") String prdtNm, @WebParam(name = "dupEventPrice") int dupEventPrice)
			throws Exception {
		List<ProductSellDTO> result = null;
		result = sellMapper.getProductList(srvcSeqc, prdtSeqc, barcode, prdtNm, dupEventPrice, PropertyHelper.summayPrevDays);
		return result;
	}

	public List<ProductSellDTO> getProductExchangeList(@WebParam(name = "srvcSeqc") int srvcSeqc,
			@WebParam(name = "prdtSeqc") int prdtSeqc, @WebParam(name = "barcode") String barcode,
			@WebParam(name = "prdtNm") String prdtNm, @WebParam(name = "dupEventPrice") int dupEventPrice)
			throws Exception {
		List<ProductSellDTO> result = null;
		result = sellMapper.getProductExchangeList(srvcSeqc, prdtSeqc, barcode, prdtNm, dupEventPrice, PropertyHelper.summayPrevDays);
		return result;
	}

	public List<CustomerDTO> getSellCustomerList(@WebParam(name = "ctmrSeqc") int ctmrSeqc,
			@WebParam(name = "ctmrNm") String ctmrNm, @WebParam(name = "mbilPhneNum") String mbilPhneNum)
			throws Exception {
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		List<CustomerDTO> result = sellMapper.getSellCustomerList(srvcSeqc, ctmrSeqc, ctmrNm, mbilPhneNum);
		return result;
	}

	public CustomerDTO getCustomer(@WebParam(name = "ctmrSeqc") int ctmrSeqc) throws Exception {
		CustomerDTO result = sellMapper.getCustomer(ctmrSeqc);
		return result;
	}

	public List<ProductSellDTO> getProductStock(@WebParam(name = "prdtMstrSeqc") int prdtMstrSeqc) throws Exception {
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		List<ProductSellDTO> result = sellMapper.getProductStock(prdtMstrSeqc, srvcSeqc, PropertyHelper.summayPrevDays);
		return result;
	}

	public List<CustodyCertificateTtalDTO> getCustodyCertificateTtal(@WebParam(name = "year") String year)
			throws Exception {
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		List<CustodyCertificateTtalDTO> result = sellMapper.getCustodyCertificateTtal(year, srvcSeqc);
		return result;
	}

	public List<CustodyCertificateDTO> getCustodyCertificate(@WebParam(name = "yymm") String yymm) throws Exception {
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		List<CustodyCertificateDTO> result = sellMapper.getCustodyCertificate(yymm, srvcSeqc);
		return result;
	}

	public SellDTO addSell(@WebParam(name = "sellDTO") SellDTO sellDTO) throws Exception {
		int sellSeqc = 0;
		int ctmrSeqc = 0;
		String receiptNo = null;
		Date regDate = null;
		// 0: 오류, 1:정상처리, 2: 판매 마스터 등록 오류, 3: 포인트 결제 오류
		// 4: 보관증 결제 오류, 5: 외상 결제 오류, 6: 판매 상세 등록 오류
		int result = 0;

		LoginDTO loginDTO = SessionHelper.getLoginDTO(request);

		// #1 판매 마스터 등록
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("sellDTO", sellDTO);
		map.put("reason", "상품 결제 사용");
		map.put("useExpnDay", loginDTO.getServiceDTO().getMaxDepositDay());
		ctmrSeqc = sellDTO.getCtmrSeqc();

		result = sellMapper.addSell(map);

		if (result != 1) {
			result = 2;
			throw new Exception("판매 마스터 등록 오류");
		}

		// #2 포인트 처리
		int usePoint = sellDTO.getPintUseMony();
		if (ctmrSeqc > 0 && usePoint > 0) {
			result = customerMapper.addUsePoint(map);

			if (result != 1) {
				result = 3;
				throw new Exception("포인트 결제 오류");
			}
		}

		// #3 보관증 처리
		int useCstdCtfc = sellDTO.getCstdCtfcUseMony();
		if (ctmrSeqc > 0 && useCstdCtfc > 0) {
			result = customerMapper.addUseCstdCtfc(map);

			if (result != 1) {
				result = 4;
				throw new Exception("보관증 결제 오류");
			}
		}

		// #4 외상 처리
		int useCredit = sellDTO.getCrdtMony();
		if (ctmrSeqc > 0 && useCredit > 0) {
			result = customerMapper.addUseCredit(map);

			if (result != 1) {
				result = 5;
				throw new Exception("외상 결제 오류");
			}
		}

		// #5 판매 상세 등록 (마스터 추가 성공 일때)
		sellSeqc = sellDTO.getSellSeqc();
		Map<String, Object> sellResult = sellMapper.getReceiptNo(sellSeqc);
		receiptNo = (String) sellResult.get("receiptNo");
		regDate = (Date) sellResult.get("regsDttm");
		Map<String, Object> detailMap = SessionHelper.getMapperParam(request);
		String pointAddReason = null;

		detailMap.put("sellSeqc", sellSeqc);
		int listSeqc = 0;

		// #6 판매 상세 입력
		for (ProductSellDTO item : sellDTO.getProductList()) {
			listSeqc++;
			item.setSellSeqc(sellSeqc);
			item.setListSeqc(listSeqc);
			detailMap.put("productSellDTO", item);
			result = sellMapper.addSellDetail(detailMap);

			if (result != 1) {
				result = 6;
				throw new Exception("판매 상세 신규 입력 오류 (상품명 : " + item.getPrdtNm() + ")");
			}
		}

		// #7 사용자 포인트 적립
		String pointYn = loginDTO.getServiceDTO().getPointYn();
		int payAmount = sellDTO.getPaymMony()
				- (sellDTO.getCrdtMony() + sellDTO.getCstdCtfcUseMony() + sellDTO.getPintUseMony());
		int pointRate = loginDTO.getServiceDTO().getPointRate();
		if (ctmrSeqc > 0 && payAmount > 0 && "Y".equals(pointYn)) {

			pointAddReason = sellDTO.getProductList().get(0).getPrdtNm();
			if (sellDTO.getProductList().size() > 1) {
				pointAddReason += " 외 " + sellDTO.getProductList().size() + "개";
			}

			map.put("ctmrSeqc", ctmrSeqc);
			map.put("payAmount", payAmount);
			map.put("pointRate", pointRate);
			map.put("pointAddReason", pointAddReason);
			int pointResult = sellMapper.addPoint(map);

			if (pointResult != 1) {
				result = 7;
				throw new Exception("포인트 적립 오류");
			}
		}

		// #8 사용자 통계 업데이트
		if (ctmrSeqc > 0) {
			map.put("ctmrSeqc", ctmrSeqc);
			CustomerDTO customerDTO = customerMapper.getCustomerStat(map);

			if (customerDTO != null && customerDTO.getCtmrSeqc() > 0) {
				map.put("customerDTO", customerDTO);
				customerMapper.setCustomerStat(map);
			}
		}

		sellDTO.setReceiptNo(receiptNo);
		sellDTO.setRegsDttm(regDate);

		return sellDTO;
	}

	public CustodyCertificateDTO getCustodyCertificateInfo(@WebParam(name = "cstdCtfcNo") String cstdCtfcNo)
			throws Exception {
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		CustodyCertificateDTO result = sellMapper.getCustodyCertificateInfo(cstdCtfcNo, srvcSeqc);
		return result;
	}

	public int setCustodyCertificate(@WebParam(name = "cstdCtfcSeqc") int cstdCtfcSeqc) throws Exception {
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		int result = sellMapper.setCustodyCertificate(cstdCtfcSeqc, srvcSeqc);
		return result;
	}

	public int delCustodyCertificate(@WebParam(name = "cstdCtfcSeqc") int cstdCtfcSeqc) throws Exception {
		int result = 0;
		int srvcSeqc = SessionHelper.getServieSeqc(request);
		result = sellMapper.delCustodyCertificate(cstdCtfcSeqc, srvcSeqc);
		return result;
	}

	public int addPay(@WebParam(name = "paymentDTO") PaymentDTO paymentDTO) throws Exception {
		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("paymentDTO", paymentDTO);
		result = sellMapper.addPay(map);
		return result;
	}

	public int setUserStat(@WebParam(name = "userSeqc") int userSeqc) throws Exception {
		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("userSeqc", userSeqc);
		CustomerDTO customerDTO = customerMapper.getCustomerStat(map);

		if (customerDTO != null && customerDTO.getCtmrSeqc() > 0) {
			result = customerMapper.setCustomer(customerDTO);
		}

		return result;
	}

	public int addReturn(@WebParam(name = "sellDTO") SellDTO sellDTO) throws Exception {
		int sellSeqc = 0;
		String receiptNo = null;
		Date regDate = null;
		// 0: 오류, 1:정상처리, 2: 판매 반품 마스터 등록 오류, 3 : 판매 반품 상세 등록 오류
		int result = 0;

		// #1 판매 반품 마스터 등록
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		sellDTO.setAddDcntMony(0);
		sellDTO.setCrdtMony(0);
		sellDTO.setCstdCtfcUseMony(0);
		sellDTO.setPintUseMony(0);
		sellDTO.setSellDvsn("2"); // 1 : 판매, 2 : 판매반품, 3 : 교환판매, 4 : 교환반품, 5 : 재고판매, 6 : 재고반품
		map.put("sellDTO", sellDTO);
		result = sellMapper.addSell(map);

		if (result != 1) {
			result = 2;
			throw new Exception("판매 반품 마스터 등록 오류");
		}

		// #2 판매 반품 정보 가져오기 (마스터 추가 성공 일때)
		sellSeqc = sellDTO.getSellSeqc();
		Map<String, Object> sellResult = sellMapper.getReceiptNo(sellSeqc);
		receiptNo = (String) sellResult.get("receiptNo");
		regDate = (Date) sellResult.get("regsDttm");
		Map<String, Object> detailMap = SessionHelper.getMapperParam(request);
		int listSeqc = 0;
		detailMap.put("sellSeqc", sellSeqc);

		// #3 판매 반품 상세 입력
		for (ProductSellDTO item : sellDTO.getProductList()) {
			listSeqc++;
			item.setSellSeqc(sellSeqc);
			item.setListSeqc(listSeqc);
			detailMap.put("productSellDTO", item);
			result = sellMapper.addSellDetail(detailMap);

			if (result != 1) {
				result = 3;
				throw new Exception("판매 반품 상세 등록 오류 (상품명 : " + item.getPrdtNm() + ")");
			}
		}

		sellDTO.setReceiptNo(receiptNo);
		sellDTO.setRegsDttm(regDate);
		return result;
	}

	public int addExchange(@WebParam(name = "sellDTO") SellDTO sellDTO,
			@WebParam(name = "oldPrdtDTO") ProductSellDTO oldPrdtDTO,
			@WebParam(name = "newPrdtDTO") ProductSellDTO newPrdtDTO) throws Exception {
		int sellSeqc = 0;
		String receiptNo = null;
		Date regDate = null;
		// 0: 오류, 1:정상처리, 2: 교환 반품 마스터 등록 오류, 3 : 교환 반품 상세 등록 오류
		// 4: 교환 판매 마스터 등록 오류, 5 : 교환 판매 상세 등록 오류
		int result = 0;

		Map<String, Object> map = SessionHelper.getMapperParam(request);
		Map<String, Object> detailMap = SessionHelper.getMapperParam(request);

		// #1 교환 반품 마스터 등록
		sellDTO.setSellDvsn("4"); // 1 : 판매, 2 : 판매반품, 3 : 교환판매, 4 : 교환반품, 5 : 재고판매, 6 : 재고반품
		map.put("sellDTO", sellDTO);
		result = sellMapper.addSell(map);

		if (result != 1) {
			result = 2;
			throw new Exception("교환 반품 마스터 등록 오류");
		}

		// #2 교환 반품 정보 가져오기 (마스터 추가 성공 일때)
		sellSeqc = sellDTO.getSellSeqc();
		Map<String, Object> sellResult = sellMapper.getReceiptNo(sellSeqc);
		receiptNo = (String) sellResult.get("receiptNo");
		regDate = (Date) sellResult.get("regsDttm");
		detailMap.put("sellSeqc", sellSeqc);

		// #3 교환 반품 상세 입력
		oldPrdtDTO.setSellSeqc(sellSeqc);
		oldPrdtDTO.setListSeqc(1);
		detailMap.put("productSellDTO", oldPrdtDTO);
		result = sellMapper.addSellDetail(detailMap);

		if (result != 1) {
			result = 3;
			throw new Exception("교환 반품 상세 등록 오류 (상품명 : " + oldPrdtDTO.getPrdtNm() + ")");
		}

		// #4 교환 판매 마스터 등록
		sellDTO.setSellDvsn("3"); // 1 : 판매, 2 : 판매반품, 3 : 교환판매, 4 : 교환반품, 5 : 재고판매, 6 : 재고반품
		map.put("sellDTO", sellDTO);

		result = sellMapper.addSell(map);

		if (result != 1) {
			result = 2;
			throw new Exception("판매 반품 마스터 등록 오류");
		}

		// #5 교환 판매 정보 가져오기 (마스터 추가 성공 일때)
		sellSeqc = sellDTO.getSellSeqc();
		sellResult = sellMapper.getReceiptNo(sellSeqc);
		receiptNo = (String) sellResult.get("receiptNo");
		regDate = (Date) sellResult.get("regsDttm");
		detailMap.put("sellSeqc", sellSeqc);

		// #6 교환 판매 상세 입력
		newPrdtDTO.setSellSeqc(sellSeqc);
		newPrdtDTO.setListSeqc(1);
		detailMap.put("productSellDTO", newPrdtDTO);
		result = sellMapper.addSellDetail(detailMap);

		if (result != 1) {
			result = 3;
			throw new Exception("교환 판매 상세 등록 오류 (상품명 : " + oldPrdtDTO.getPrdtNm() + ")");
		}

		sellDTO.setReceiptNo(receiptNo);
		sellDTO.setRegsDttm(regDate);
		return result;
	}

	public List<SellDTO> getReceiptList(@WebParam(name = "receiptNo") String receiptNo,
			@WebParam(name = "sellDtStart") String sellDtStart, @WebParam(name = "sellDtEnd") String sellDtEnd,
			@WebParam(name = "ctmrNm") String ctmrNm, @WebParam(name = "prdtNm") String prdtNm) throws Exception {
		List<SellDTO> result = null;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("receiptNo", receiptNo);
		map.put("sellDtStart", sellDtStart);
		map.put("sellDtEnd", sellDtEnd);
		map.put("ctmrNm", ctmrNm);
		map.put("prdtNm", prdtNm);
		result = sellMapper.getSellList(map);
		return result;
	}

	public List<ProductSellDTO> getReceiptProductList(@WebParam(name = "sellSeqc") int sellSeqc) throws Exception {
		List<ProductSellDTO> result = null;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("sellSeqc", sellSeqc);
		result = sellMapper.getReceiptProductList(map);
		return result;
	}

	public List<PaymentDTO> getPaymentList(@WebParam(name = "sellSeqc") int sellSeqc) throws Exception {
		List<PaymentDTO> result = null;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("sellSeqc", sellSeqc);
		result = sellMapper.getPaymentList(map);
		return result;
	}

	public int getNewCstdCtfcSeqc() throws Exception {
		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("cstdCtfcSeqc", 0);
		map.put("markMony", 0);
		map.put("useExpnDay", null);
		map.put("useYn", "N"); // 번호 생성시 N 최종 확정시 Y : N 상태의 보관증은 사용안함
		sellMapper.addCstdCtfc(map);
		result = (int) map.get("cstdCtfcSeqc");
		return result;
	}

	public int setCstdCtfc(@WebParam(name = "cstdCtfcSeqc") int cstdCtfcSeqc, @WebParam(name = "markMony") int markMony,
			@WebParam(name = "useExpnDay") String useExpnDay) throws Exception {
		int result = 0;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("cstdCtfcSeqc", cstdCtfcSeqc);
		map.put("markMony", markMony);
		map.put("useExpnDay", useExpnDay);
		map.put("useYn", "Y"); // 번호 생성시 N 최종 확정시 Y : N 상태의 보관증은 사용안함
		result = sellMapper.setCstdCtfc(map);
		return result;
	}

	public List<ProductSellDTO> getSellProductList(@WebParam(name = "sellDtStart") String sellDtStart,
			@WebParam(name = "sellDtEnd") String sellDtEnd, @WebParam(name = "groupBy") int groupBy,
			@WebParam(name = "accountGroupYn") String accountGroupYn) throws Exception {
		List<ProductSellDTO> result = null;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("sellDtStart", sellDtStart);
		map.put("sellDtEnd", sellDtEnd);
		map.put("groupBy", groupBy);
		map.put("accountGroupYn", accountGroupYn);
		result = sellMapper.getSellProductList(map);
		return result;
	}

	public List<ProductSellHistoryDTO> getSellHistory(@WebParam(name = "sellDt") String sellDt,
			@WebParam(name = "sellDvsn") String sellDvsn) throws Exception {
		List<ProductSellHistoryDTO> result = null;
		Map<String, Object> map = SessionHelper.getMapperParam(request);
		map.put("sellDt", sellDt);
		map.put("sellDvsn", sellDvsn);
		result = sellMapper.getSellHistory(map);
		return result;
	}

}
