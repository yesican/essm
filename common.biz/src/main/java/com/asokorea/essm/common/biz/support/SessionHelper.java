package com.asokorea.essm.common.biz.support;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;

import com.asokorea.essm.common.biz.dto.LoginDTO;

public final class SessionHelper {
	
	public static final String LOGIN_ATTRIBTE_NAME = "LOGIN_INFO"; 
	public static final String LOGIN_IP = "LOGIN_IP"; 
	public static final String LOGIN_MAC = "LOGIN_MAC";
	public static Map<String, LoginDTO> sessionMap = new HashMap<>();
	
	static public HttpSession getSession(HttpServletRequest request){
		HttpSession session = request.getSession(true);
		return session;
	}
	
	static public LoginDTO getLoginDTO(HttpServletRequest request){
		LoginDTO result = null;
		HttpSession session = getSession(request);
		
		if(session != null && session.getAttribute(LOGIN_ATTRIBTE_NAME) != null){
			result = (LoginDTO) session.getAttribute(LOGIN_ATTRIBTE_NAME);
		}
		
		return result;
	}
	
	static public void setLoginDTO(LoginDTO loginDTO, HttpServletRequest request){
		HttpSession session = getSession(request);
		
		if(session != null && loginDTO != null){
			session.setAttribute(LOGIN_ATTRIBTE_NAME, loginDTO);
			sessionMap.put(loginDTO.getUserId(), loginDTO);
		}
		
	}
	
	static public int getServieSeqc(HttpServletRequest request){
		int result = 0;
		LoginDTO loginDTO = getLoginDTO(request);
		
		if(loginDTO != null){
			result = loginDTO.getSrvcSeqc();
		}
		return result;
	}
	
	static public String getUserId(HttpServletRequest request){
		String result = null;
		LoginDTO loginDTO = getLoginDTO(request);
		
		if(loginDTO != null){
			result = loginDTO.getUserId();
		}
		return result;
	}

	public static String getLoginIP(HttpServletRequest request) {
		String result = null;
		HttpSession session = getSession(request);
		
		if(session != null && session.getAttribute(LOGIN_IP) != null){
			result = (String) session.getAttribute(LOGIN_IP);
		}
		
		return result;
	}

	public static String getLoginMAC(HttpServletRequest request) {
		String result = null;
		HttpSession session = getSession(request);
		
		if(session != null && session.getAttribute(LOGIN_MAC) != null){
			result = (String) session.getAttribute(LOGIN_MAC);
		}
		
		return result;
	}

	static public Map<String, Object> getMapperParam(HttpServletRequest request){
		LoginDTO loginDTO = null;
		Map<String, Object> result = new HashMap<String, Object>();
		
		loginDTO = getLoginDTO(request);
		
		if(loginDTO != null){
			result.put("userId", loginDTO.getUserId());
			result.put("srvcSeqc", loginDTO.getSrvcSeqc());
			
			result.put("dupEventPrice", loginDTO.getServiceDTO().getDupEventPrice());
			result.put("summayPrevDays", PropertyHelper.summayPrevDays);
			result.put("loginIP", getLoginIP(request));
			result.put("loginMAC", getLoginMAC(request));
		}
		
		return result;
	}

}
