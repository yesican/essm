package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="ProductSellHistoryDTO")
public class ProductSellHistoryDTO {
	
	private int sellSeqc;
	private Date sellDt;
	private String sellDvsn;
	private int listSeqc;
	private int prdtMstrSeqc;
	private int prdtSeqc;
	private String prdtNm;
	private String clor;
	private String size;
	private int sellPrce;
	private int sellQntt;
	private String barcode;
	private String receiptNo;
	
	public int getSellSeqc() {
		return sellSeqc;
	}
	public void setSellSeqc(int sellSeqc) {
		this.sellSeqc = sellSeqc;
	}
	public Date getSellDt() {
		return sellDt;
	}
	public void setSellDt(Date sellDt) {
		this.sellDt = sellDt;
	}
	public String getSellDvsn() {
		return sellDvsn;
	}
	public void setSellDvsn(String sellDvsn) {
		this.sellDvsn = sellDvsn;
	}
	public int getListSeqc() {
		return listSeqc;
	}
	public void setListSeqc(int listSeqc) {
		this.listSeqc = listSeqc;
	}
	public int getPrdtMstrSeqc() {
		return prdtMstrSeqc;
	}
	public void setPrdtMstrSeqc(int prdtMstrSeqc) {
		this.prdtMstrSeqc = prdtMstrSeqc;
	}
	public int getPrdtSeqc() {
		return prdtSeqc;
	}
	public void setPrdtSeqc(int prdtSeqc) {
		this.prdtSeqc = prdtSeqc;
	}
	public String getPrdtNm() {
		return prdtNm;
	}
	public void setPrdtNm(String prdtNm) {
		this.prdtNm = prdtNm;
	}
	public String getClor() {
		return clor;
	}
	public void setClor(String clor) {
		this.clor = clor;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public int getSellPrce() {
		return sellPrce;
	}
	public void setSellPrce(int sellPrce) {
		this.sellPrce = sellPrce;
	}
	public int getSellQntt() {
		return sellQntt;
	}
	public void setSellQntt(int sellQntt) {
		this.sellQntt = sellQntt;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
}
