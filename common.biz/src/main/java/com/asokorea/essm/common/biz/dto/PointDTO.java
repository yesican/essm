package com.asokorea.essm.common.biz.dto;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="PointDTO")
public class PointDTO {

	private int ctmrPintSeqc;
	private int ctmrSeqc;
	private int sellSeqc;
	private String accmSbttDvsn;
	private int point;
	private String accmSbttRasn;
	private String ocrcDay;
	
	public int getCtmrPintSeqc() {
		return ctmrPintSeqc;
	}
	public void setCtmrPintSeqc(int ctmrPintSeqc) {
		this.ctmrPintSeqc = ctmrPintSeqc;
	}
	public int getCtmrSeqc() {
		return ctmrSeqc;
	}
	public void setCtmrSeqc(int ctmrSeqc) {
		this.ctmrSeqc = ctmrSeqc;
	}
	public int getSellSeqc() {
		return sellSeqc;
	}
	public void setSellSeqc(int sellSeqc) {
		this.sellSeqc = sellSeqc;
	}
	public String getAccmSbttDvsn() {
		return accmSbttDvsn;
	}
	public void setAccmSbttDvsn(String accmSbttDvsn) {
		this.accmSbttDvsn = accmSbttDvsn;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public String getAccmSbttRasn() {
		return accmSbttRasn;
	}
	public void setAccmSbttRasn(String accmSbttRasn) {
		this.accmSbttRasn = accmSbttRasn;
	}
	public String getOcrcDay() {
		return ocrcDay;
	}
	public void setOcrcDay(String ocrcDay) {
		this.ocrcDay = ocrcDay;
	}

}
