package com.asokorea.essm.common.biz.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="CustomerDTO")
public class CustomerDTO {

	private int ctmrSeqc;
	private int srvcSeqc;
	private String ctmrNm;
	private String sex;
	private String brthDay;
	private String mbilPhneNum;
	private String smsYn;
	private String mailAddr;
	private String mailRcivYn;
	private String addr;
	private String frstBuyDay;
	private String rcntBuyDay;
	private int ttalBuyMony;
	private int ttalBuyCnt;
	private int ttalPint;
	private int ttalCrdt;
	private String useYn;
	private String sort;
	private String regsUserId;
	private Date regsDttm;
	private String updtUserId;
	private Date updtDttm;
	
	public int getCtmrSeqc() {
		return ctmrSeqc;
	}
	public void setCtmrSeqc(int ctmrSeqc) {
		this.ctmrSeqc = ctmrSeqc;
	}
	public int getSrvcSeqc() {
		return srvcSeqc;
	}
	public void setSrvcSeqc(int srvcSeqc) {
		this.srvcSeqc = srvcSeqc;
	}
	public String getCtmrNm() {
		return ctmrNm;
	}
	public void setCtmrNm(String ctmrNm) {
		this.ctmrNm = ctmrNm;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBrthDay() {
		return brthDay;
	}
	public void setBrthDay(String brthDay) {
		this.brthDay = brthDay;
	}
	public String getMbilPhneNum() {
		return mbilPhneNum;
	}
	public void setMbilPhneNum(String mbilPhneNum) {
		this.mbilPhneNum = mbilPhneNum;
	}
	public String getSmsYn() {
		return smsYn;
	}
	public void setSmsYn(String smsYn) {
		this.smsYn = smsYn;
	}
	public String getMailAddr() {
		return mailAddr;
	}
	public void setMailAddr(String mailAddr) {
		this.mailAddr = mailAddr;
	}
	public String getMailRcivYn() {
		return mailRcivYn;
	}
	public void setMailRcivYn(String mailRcivYn) {
		this.mailRcivYn = mailRcivYn;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getFrstBuyDay() {
		return frstBuyDay;
	}
	public void setFrstBuyDay(String frstBuyDay) {
		this.frstBuyDay = frstBuyDay;
	}
	public String getRcntBuyDay() {
		return rcntBuyDay;
	}
	public void setRcntBuyDay(String rcntBuyDay) {
		this.rcntBuyDay = rcntBuyDay;
	}
	public int getTtalBuyMony() {
		return ttalBuyMony;
	}
	public void setTtalBuyMony(int ttalBuyMony) {
		this.ttalBuyMony = ttalBuyMony;
	}
	public int getTtalBuyCnt() {
		return ttalBuyCnt;
	}
	public void setTtalBuyCnt(int ttalBuyCnt) {
		this.ttalBuyCnt = ttalBuyCnt;
	}
	public int getTtalPint() {
		return ttalPint;
	}
	public void setTtalPint(int ttalPint) {
		this.ttalPint = ttalPint;
	}
	public int getTtalCrdt() {
		return ttalCrdt;
	}
	public void setTtalCrdt(int ttalCrdt) {
		this.ttalCrdt = ttalCrdt;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getRegsUserId() {
		return regsUserId;
	}
	public void setRegsUserId(String regsUserId) {
		this.regsUserId = regsUserId;
	}
	public Date getRegsDttm() {
		return regsDttm;
	}
	public void setRegsDttm(Date regsDttm) {
		this.regsDttm = regsDttm;
	}
	public String getUpdtUserId() {
		return updtUserId;
	}
	public void setUpdtUserId(String updtUserId) {
		this.updtUserId = updtUserId;
	}
	public Date getUpdtDttm() {
		return updtDttm;
	}
	public void setUpdtDttm(Date updtDttm) {
		this.updtDttm = updtDttm;
	}
	
}
