package com.asokorea.essm.common.biz.dao.essm;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.asokorea.essm.common.biz.dto.BuyDetailRegsDTO;
import com.asokorea.essm.common.biz.dto.BuyRegsDTO;
import com.asokorea.essm.common.biz.dto.OrderBuyMainDTO;
import com.asokorea.essm.common.biz.dto.OrdrBuyDetailDTO;
import com.asokorea.essm.common.biz.dto.OrdrDcmtRegsDTO;
import com.asokorea.essm.common.biz.dto.OrdrRegsDTO;
import com.asokorea.essm.common.biz.dto.OrdrRegsSearchDTO;
import com.asokorea.essm.common.biz.dto.ProductColListDTO;
import com.asokorea.essm.common.biz.dto.ProductSizeListDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtDetailRegsDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtProdSearchDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtRegsDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtSellDTO;
import com.asokorea.essm.common.biz.dto.StckAjmtSellDetailDTO;
import com.asokorea.essm.common.biz.dto.StockListDTO;
import com.asokorea.essm.common.biz.dto.StockListSearchDTO;

@Mapper
public interface StockMapper {

	public List<OrdrRegsDTO> getOrdrRegsList(
			@Param("ordrRegsSearchDTO") OrdrRegsSearchDTO ordrRegsSearchDTO,
			@Param("summayPrevDays") int summayPrevDays) throws Exception;
	
	public int addOrderDocument(@Param("ordrDcmtRegsDTO") OrdrDcmtRegsDTO ordrDcmtRegsDTO) throws Exception;
	
	public int addOrderDocumentDetail(@Param("ordrRegsDTO") OrdrRegsDTO ordrRegsDTO) throws Exception;
	
	public List<OrdrRegsDTO> getBasketList(@Param("srvcSeqc") int srvcSeqc, @Param("summayPrevDays") int summayPrevDays) throws Exception;
	
	public int addBuyBasket(Map<String, Object> map) throws Exception;
	
	public int delBasket(@Param("prdtSeqc") int prdtSeqc, @Param("srvcSeqc") int srvcSeqc) throws Exception;
	
	public int setBasketUseYn(Map<String, Object> map) throws Exception;
	
	public List<BuyRegsDTO> getBuyList(@Param("buyDay") String buyDay, @Param("srvcSeqc") int srvcSeqc) throws Exception;
	
	public List<BuyDetailRegsDTO> getBuyDetailList(@Param("buySeqc") int buySeqc) throws Exception;
	
	public List<ProductColListDTO> getBuyRegsClorList(@Param("prdtMstrSeqc") int prdtMstrSeqc) throws Exception;
	
	public List<ProductSizeListDTO> getBuyRegsSzieList(@Param("prdtMstrSeqc") int prdtMstrSeqc) throws Exception;
	
	public List<BuyDetailRegsDTO> getBuyRegsProductList(@Param("srvcSeqc") int srvcSeqc, @Param("prdtNm") String prdtNm) throws Exception;
	
	public int addBuy(@Param("buyRegsDTO") BuyRegsDTO buyRegsDTO) throws Exception;
	
	public int setBuy(@Param("buyRegsDTO") BuyRegsDTO buyRegsDTO) throws Exception;
	
	public int addBuyDetail(@Param("buyDetailRegsDTO") BuyDetailRegsDTO buyDetailRegsDTO) throws Exception;
	
	public int setBuyDetail(@Param("buyDetailRegsDTO") BuyDetailRegsDTO buyDetailRegsDTO) throws Exception;
	
	public int addOrSetBuyDetail(@Param("buyDetailRegsDTO") BuyDetailRegsDTO buyDetailRegsDTO) throws Exception;
	
	public int delBuyDetail(@Param("buyDtlSeqc") int buyDtlSeqc) throws Exception;
	
	public List<BuyDetailRegsDTO> getBuyProductSetList(@Param("buySeqc") int buySeqc, @Param("prdtMstrSeqc") int prdtMstrSeqc) throws Exception;
	
	public int getBuyProductPrdtSeqc(@Param("prdtNm") String prdtNm, @Param("clor") String clor, @Param("size") String size) throws Exception;
	
	public List<OrderBuyMainDTO> getOrderBuyMainList(@Param("mainAcntSeqc") int mainAcntSeqc, 
			@Param("strtOrdrDay") String strtOrdrDay, @Param("endOrdrDay") String endOrdrDay) throws Exception;
	
	public List<OrdrBuyDetailDTO> getOrderBuyDetailList(@Param("mainAcntSeqc") int mainAcntSeqc, 
			@Param("ordrDay") String ordrDay) throws Exception;
	
	public int setOrderDocumentDetails(@Param("ordrBuyDetailDTO") OrdrBuyDetailDTO ordrBuyDetailDTO, 
			@Param("ordrDay") String ordrDay,
			@Param("mainAcntSeqc") int mainAcntSeqc) throws Exception;
	
	public int addOrderBuy(@Param("buyRegsDTO") BuyRegsDTO buyRegsDTO, 
			@Param("buyDetailRegsDTO") BuyDetailRegsDTO buyDetailRegsDTO,
			@Param("ordrBuyDetailDTO") OrdrBuyDetailDTO ordrBuyDetailDTO,
			@Param("ordrDay") String ordrDay,
			@Param("mainAcntSeqc") int mainAcntSeqc) throws Exception;
	
	public List<StockListDTO> getStockList(@Param("stockListSearchDTO") StockListSearchDTO stockListSearchDTO) throws Exception;
	
	public List<StckAjmtRegsDTO> getStckAjmtList(@Param("year") String year, @Param("srvcSeqc") int srvcSeqc) throws Exception;
	
	public List<StckAjmtDetailRegsDTO> getStckAjmtDetailList(@Param("stckAjmtSeqc") int stckAjmtSeqc, 
			@Param("summayPrevDays") int summayPrevDays) throws Exception;
	
	public List<StckAjmtDetailRegsDTO> getStckAjmtProdList(@Param("stckAjmtProdSearchDTO") StckAjmtProdSearchDTO stckAjmtProdSearchDTO,
			@Param("summayPrevDays") int summayPrevDays) throws Exception;
	
	public int addStckAjmt(@Param("stckAjmtRegsDTO") StckAjmtRegsDTO stckAjmtRegsDTO) throws Exception;
	
	public int setStckAjmt(@Param("stckAjmtRegsDTO") StckAjmtRegsDTO stckAjmtRegsDTO) throws Exception;
	
	public int addStckAjmtDetail(@Param("stckAjmtDetailRegsDTO") StckAjmtDetailRegsDTO stckAjmtDetailRegsDTO) throws Exception;
	
	public int setStckAjmtDetail(@Param("stckAjmtDetailRegsDTO") StckAjmtDetailRegsDTO stckAjmtDetailRegsDTO) throws Exception;
	
	public int addStckAjmtSell(@Param("stckAjmtSellDTO") StckAjmtSellDTO stckAjmtSellDTO) throws Exception;
	
	public int addStckAjmtSellDetail(@Param("stckAjmtSellDetailDTO") StckAjmtSellDetailDTO stckAjmtSellDetailDTO) throws Exception;
	
	public List<StckAjmtDetailRegsDTO> getStckAjmtProdBarcodeList(@Param("srvcSeqc") int srvcSeqc, @Param("barcode") String barcode,
			@Param("summayPrevDays") int summayPrevDays) throws Exception;

	public int addBuyPay(Map<String, Object> map) throws Exception;
	
	public int setBuyPay(Map<String, Object> map) throws Exception;
}